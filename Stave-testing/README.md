# Stave testing 

The purpose of this directory is to store documentation for performing stave testing at BNL. 

Version 1 of the software used for stave testing can be found [here](https://gitlab.cern.ch/grosin/radiation). Private forked version which has more updated files w.r.t. main repo can be found [here](https://gitlab.cern.ch/atishelm/radiation/-/tree/AbeForkBranch). 

## Run commands 

To run the humidity and temperature measurements to be tracked on grafana, first start influxdb in one terminal (check raspberry pi influxdb settings):

```
sudo influxd
```

Then in another terminal, navigate to this directory and run a command of the format:

```
python3 usbtc08_logger.py log <NumberOfSeconds> <interval>
```

Where `<NumberOfSeconds>` is the number of seconds to run the logger for, and `<interval>` determines the measurement interval in ms. An example command to run or 5 minutes with 1ms intervals is:

```
python3 usbtc08_logger.py log 300 1
```

Current IP and location of the grafana dashboard: 

http://10.2.252.213:3000/d/tWZO1qkgk/stave-dcs?orgId=1&refresh=5s
