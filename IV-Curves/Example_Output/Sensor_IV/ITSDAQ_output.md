# Running a sensor IV from 0V to -100V with -10V steps

bash
```
TkHV::IVScan completed for iseg at resource /dev/serial/by-id/usb-Linux_4.9.220-2.8.7+g57229263ff65_with_2184000.usb_Gadget_Serial_v2.4-if00
Saving pdf to file /home/qcbox/Desktop/itsdaq-sw-6March/itsdaq-sw/DAT//ps/VPX_W00000_IV_197.pdf
Info in <TCanvas::Print>: pdf file /home/qcbox/Desktop/itsdaq-sw-6March/itsdaq-sw/DAT//ps/VPX_W00000_IV_197.pdf has been created
Saving root to file /home/qcbox/Desktop/itsdaq-sw-6March/itsdaq-sw/DAT//data/VPX_W00000_IV_197.root
Info in <TCanvas::SaveAs>: ROOT file /home/qcbox/Desktop/itsdaq-sw-6March/itsdaq-sw/DAT//data/VPX_W00000_IV_197.root has been created
Saving ascii to file /home/qcbox/Desktop/itsdaq-sw-6March/itsdaq-sw/DAT//results/VPX_W00000_IV_197.dat
```
