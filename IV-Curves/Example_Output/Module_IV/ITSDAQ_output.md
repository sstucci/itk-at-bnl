# Module IV from 0V to -100V with -10V steps, ITSDAQ output

```bash
ST [1] HVSupplies[0]->SetLimit(-550)
TkHV::SetLimit: limit set to 550
(bool) true
ST [2] HandleMenu completed for id 117 in 0.00296307 seconds
Warning! Incorrect component format entered! Enter a format of 20USxxxxxxxxxx
Warnings found! Press OK to proceed anyways.
Warning! Incorrect component format entered! Enter a format of 20USxxxxxxxxxx
0	48	
9.89011	59	
19.7802	69	
29.6703	79	
39.9267	90	
49.8168	101	
59.707	111	
69.9634	122	
79.8535	132	
89.7436	143	
100	154	
109.89	164	
119.78	174	
129.67	185	
139.927	195	
149.817	206	
159.707	217	
169.963	228	
179.853	238	
189.744	248	
200	259	
209.89	269	
219.78	280	
229.67	291	
239.927	302	
249.817	312	
259.707	323	
269.963	333	
279.853	344	
289.744	354	
300	365	
309.89	375	
319.78	386	
329.67	397	
339.927	407	
349.817	417	
359.707	428	
369.963	440	
379.853	450	
389.744	460	
400	471	
409.89	481	
419.78	492	
429.67	502	
439.927	513	
449.817	524	
459.707	534	
469.963	545	
479.853	555	
489.744	566	
500	577	
509.89	588	
519.78	598	
529.67	608	
539.927	619	
549.817	630	
559.707	639	
569.963	650	
579.853	661	
589.744	671	
600	683	
609.89	693	
619.78	703	
629.67	714	
639.927	725	
649.817	735	
659.707	746	
669.963	759	
679.854	770	
689.744	780	
699.634	790	
709.89	801	
719.78	811	
729.67	822	
739.927	832	
749.817	843	
759.707	854	
769.963	863	
779.853	874	
789.744	884	
800	896	
809.89	906	
819.78	916	
829.67	927	
839.927	937	
849.817	948	
859.707	958	
869.963	969	
879.854	979	
889.744	989	
899.634	1000	
909.89	1011	
919.78	1021	
929.67	1023	
939.927	1023	
949.817	1023	
959.707	1023	
969.963	1023	
979.853	1023	
989.744	1023	
1000	1023	

****************************************
Minimizer is Minuit / Migrad
Chi2                      =      6.10785
NDf                       =           41
Edm                       =  7.53851e-20
NCalls                    =           42
p0                        =     0.945786   +/-   0.000448594 
p1                        =     -45.2811   +/-   0.134562    
AM0	= 43
AM1	= 45
AM2	= 43
AM3	= 37
AM4	= 46
AM5	= 51
AM6	= 49
AM7	= 134
AM8	= 44
AM9	= 40
AM10	= 37
AM11	= 46
AM12	= 40
AM13	= 36
AM14	= 45
AM15	= 38
NTC0 Vref	=243.066879
NTC1 Vref	=247.795807
NTC2 Vref	=250.633163
Scheduled job 1
TkHV::RampImpl ramping to 0V, rampRate 3
TkHV::SetDelay not yet implemented for iseg
Requested 0V Achieved -0.12V current 0.14uA
TkHV::Mon for iseg at resource /dev/serial/by-id/usb-Linux_4.9.220-2.8.7+g57229263ff65_with_2184000.usb_Gadget_Serial_v2.4-if00 label Module 0
Cannot Monitor values if source output is off. Returning zeros.
No compliance implemented for ISEG
Scheduled job 2
TkHV::RampImpl ramping to -50V, rampRate 3
TkHV::SetDelay not yet implemented for iseg
Requested -2.629V Achieved -0.128V current 0.139uA
Requested -5.129V Achieved -4.886V current -4.448uA
Requested -7.629V Achieved -6.284V current -3.59003uA
Requested -10.129V Achieved -9.918V current -3.59003uA
Requested -12.629V Achieved -12.402V current -2.63384uA
Requested -15.129V Achieved -14.909V current -3.60837uA
Requested -17.629V Achieved -17.411V current -3.35144uA
Requested -20.129V Achieved -19.905V current -4.34095uA
Requested -22.629V Achieved -22.412V current -4.08726uA
Requested -25.129V Achieved -24.9V current -3.80986uA
Requested -27.629V Achieved -27.404V current -4.75888uA
Requested -30.129V Achieved -29.899V current -4.46154uA
Requested -32.629V Achieved -32.404V current -5.46312uA
Requested -35.129V Achieved -34.908V current -5.25602uA
Requested -37.629V Achieved -34.905V current -3.64634uA
Requested -40.129V Achieved -39.773V current -7.32279uA
Requested -42.629V Achieved -39.904V current -4.45784uA
Requested -45.129V Achieved -44.912V current -7.10686uA
Requested -47.629V Achieved -44.908V current -5.03294uA
Requested -50V Achieved -44.907V current -4.41247uA
TkHV::SetDelay not yet implemented for iseg
No compliance implemented for ISEG
Scheduled job 3
TkHV::RampImpl ramping to -100V, rampRate 3
TkHV::SetDelay not yet implemented for iseg
Requested -47.406V Achieved -46.686V current -5.45216uA
Requested -49.906V Achieved -49.703V current -6.00773uA
Requested -52.406V Achieved -52.204V current -7.10165uA
Requested -54.906V Achieved -54.702V current -6.89082uA
Requested -57.406V Achieved -57.206V current -6.74034uA
Requested -59.906V Achieved -59.694V current -7.76498uA
Requested -62.406V Achieved -62.197V current -7.51271uA
Requested -64.906V Achieved -64.701V current -7.32224uA
Requested -67.406V Achieved -67.197V current -8.23805uA
Requested -69.906V Achieved -69.701V current -7.9528uA
Requested -72.406V Achieved -72.193V current -8.88129uA
Requested -74.906V Achieved -74.696V current -8.57145uA
Requested -77.406V Achieved -77.194V current -9.56693uA
Requested -79.906V Achieved -79.698V current -9.32989uA
Requested -82.406V Achieved -82.203V current -9.1061uA
Requested -84.906V Achieved -84.693V current -10.0789uA
Requested -87.406V Achieved -84.69V current -8.21941uA
Requested -89.906V Achieved -89.694V current -10.9587uA
Requested -92.406V Achieved -89.689V current -9.02173uA
Requested -94.906V Achieved -94.686V current -11.8299uA
Requested -97.406V Achieved -94.682V current -9.56369uA
Requested -99.906V Achieved -99.686V current -12.6413uA
Requested -100V Achieved -99.683V current -10.0959uA
TkHV::SetDelay not yet implemented for iseg
No compliance implemented for ISEG
Scheduled job 4
TkHV::RampImpl ramping to -150V, rampRate 3
TkHV::SetDelay not yet implemented for iseg
Requested -102.606V Achieved -102.714V current -11.1443uA
Requested -105.106V Achieved -105.209V current -11.1205uA
Requested -107.606V Achieved -107.71V current -12.159uA
Requested -110.106V Achieved -110.21V current -11.9249uA
Requested -112.606V Achieved -112.712V current -11.6866uA
Requested -115.106V Achieved -115.203V current -12.6374uA
Requested -117.606V Achieved -117.708V current -12.3755uA
Requested -120.106V Achieved -120.198V current -13.3464uA
Requested -122.606V Achieved -122.707V current -13.1241uA
Requested -125.106V Achieved -125.209V current -12.895uA
Requested -127.606V Achieved -127.695V current -13.8519uA
Requested -130.106V Achieved -130.2V current -13.5486uA
Requested -132.606V Achieved -132.697V current -14.4804uA
Requested -135.106V Achieved -135.203V current -14.2127uA
Requested -137.606V Achieved -137.698V current -14.007uA
Requested -140.106V Achieved -140.204V current -14.9968uA
Requested -142.606V Achieved -142.71V current -14.7367uA
Requested -145.106V Achieved -145.207V current -14.5797uA
Requested -147.606V Achieved -147.712V current -15.5917uA
Requested -150V Achieved -147.71V current -13.8751uA
TkHV::SetDelay not yet implemented for iseg
No compliance implemented for ISEG
Scheduled job 5
TkHV::RampImpl ramping to -200V, rampRate 3
TkHV::SetDelay not yet implemented for iseg
Requested -150.21V Achieved -149.389V current -14.7364uA
Requested -152.71V Achieved -152.808V current -15.2207uA
Requested -155.21V Achieved -155.3V current -16.2649uA
Requested -157.71V Achieved -157.806V current -16.0063uA
Requested -160.21V Achieved -160.291V current -15.8006uA
Requested -162.71V Achieved -162.796V current -16.7625uA
Requested -165.21V Achieved -165.3V current -16.5173uA
Requested -167.71V Achieved -167.799V current -16.3422uA
Requested -170.21V Achieved -170.309V current -17.276uA
Requested -172.71V Achieved -172.797V current -16.9946uA
Requested -175.21V Achieved -175.303V current -17.979uA
Requested -177.71V Achieved -177.799V current -17.6489uA
Requested -180.21V Achieved -180.3V current -18.6184uA
Requested -182.71V Achieved -182.809V current -18.3504uA
Requested -185.21V Achieved -185.295V current -18.1163uA
Requested -187.71V Achieved -187.799V current -19.0719uA
Requested -190.21V Achieved -190.296V current -18.8355uA
Requested -192.71V Achieved -192.8V current -18.6478uA
Requested -195.21V Achieved -195.287V current -19.5579uA
Requested -197.71V Achieved -197.792V current -20.286uA
Requested -200V Achieved -197.792V current -20.286uA
TkHV::SetDelay not yet implemented for iseg
AMAC_IVScan: will repeat final voltage for 3 more measurements
No compliance implemented for ISEG
No compliance implemented for ISEG
No compliance implemented for ISEG
No compliance implemented for ISEG
Creating json file for results (_PPA)
AMAC_IVScan completed
Writing data into output file
At least one AMAC is on
Please turn off before reading PTAT zero
     VDCDC:  1324.1
    DCDCin: 10994.8
    NTCx_V: 659.213
      NTCx:  22.094
    NTCy_V: 247.796
      NTCy: -273.15
   NTCpb_V: 800.135
     NTCpb: 28.4623
      CTAT: 453.977
    Cur1V5: 478.567
   HVret_V: 7.56628
     HVret: 58.6387
    PTAT_V: 1288.16
      PTAT:   265.6
     Hrefx: 2.83736
     Hrefy:       0
       CAL: -1.89157
  VDDLRlow: 766.086
   AM900BG: 875.797
   AM600BG: 592.062
 VDDLRhigh: 766.086
    Cur10V:  933.49
      VREG: 1193.11
     AMref: 105.928
   CHIPGND:       0
      HGND:       0
     VDDHI: 3325.38
 DCDCinLow: 832.291
DCDCinHigh: 872.014
DCDCoutLow: 860.665
DCDCoutHigh: 872.014
     BG600: 631.785
      CALx: 14.1868
      CALy: 12.2952
    Shuntx:       0
    Shunty:       0
Data not taken at 500V
File /home/qcbox/Desktop/itsdaq-sw-6March/itsdaq-sw/DAT/results/SNUS12341111_R5M1_HALFMODULE_20230314_203_1_MODULE_IV_AMAC.json opened to save results
```
