"""
9 March 2023
Abraham Tishelman-Charny

The purpose of this python module is to plot IV curves. To later be integrated in more general ITk plotter.

Example usage:
python3 IV_Curves.py --inFile "data/20USBML1234791_R5M1_HALFMODULE_20230308_146_1_MODULE_IV_AMAC.json" --IV_Type module
"""

# imports 
import json 
import pandas as pd 
import numpy as np 
import os 
import csv 
from matplotlib import pyplot as plt 
import mplhep as hep 
plt.style.use(hep.style.ATLAS) # customization: https://mplhep.readthedocs.io/en/latest/api.html
#from python.IV_Curves_Tools import * 
from python.SetOptions import options 

# Command line flag options 
inFile = options.inFile
IV_Type = options.IV_Type
ol = options.ol 
debug = options.debug

# Create output directory if it doesn't already exist
if(not os.path.exists(ol)):
    print("Creating output directory:",ol)
    os.system("mkdir -p %s"%(ol))
    os.system("cp %s/../index.php %s"%(ol, ol)) # assumes one directory up from 'ol' exists

# Functions 
def Save_Figures(fig, fileTypes_, OUTNAME_, plotType):
    for fileType in fileTypes_:
        outname = "%s_%s.%s"%(OUTNAME_, plotType, fileType)
        print("")
        print("Saving figure:",outname)
        print("")
        fig.savefig(outname)
    plt.close()    

def Make_IV_Plot(VOLTAGE_vals_, CURRENT_vals_, CURRENT_RMS_vals_, PS_CURRENT_vals_, x_label_, y_label_, OUTNAME_, component_, TEMPERATURE_, debug):
    unit_dict = {
        "Current" : "$\mu$A",
        "Voltage" : "mV",
        "Gain" : "",
        "vt50" : "",
        "Input_noise" : r"$e^{-}$"
    }

    component_dict = {
        "20USBML1234791" : "BNL-PPB2-MLS-111"
    }

    if("USBML" in component_): humanReadableName = component_dict[component_]
    else: humanReadableName = component_

    if(debug): print("CURRENT_RMS_vals_:",CURRENT_RMS_vals_)

    #yunit = unit_dict[Value_type_]
    xmin, xmax = min(VOLTAGE_vals_), max(VOLTAGE_vals_)

    # Subtract the y intercept from all values (offset)
    val_0 = float(CURRENT_vals_[0])
    CURRENT_vals_ = [float(val_i - val_0) for val_i in CURRENT_vals_]

    # params 
    fileTypes = ["png", "pdf"]
    Nentries = len(CURRENT_vals_)
    #ymaxIncrement = 0.333
    ymaxIncrement = 0.425

    # IV curve plot 
    fig, ax = plt.subplots(1, figsize=(6, 4), dpi=100)

    PS_ax = ax.twinx()
    AMAC_data_color = "C0"
    PS_data_color = "C1"

    ax.errorbar(VOLTAGE_vals_, CURRENT_vals_, yerr=CURRENT_RMS_vals_, xerr=None, marker='o', markersize=2, elinewidth=1, linewidth=0.5)
    hep.atlas.text("ITk Strips Internal")
    ax.set_xlabel(x_label_)
    ax.set_ylabel(y_label_, color=AMAC_data_color)
    ax.spines['left'].set_color(AMAC_data_color)
    ax.spines['right'].set_color(PS_data_color)
    ax.yaxis.label.set_color(AMAC_data_color)
    ax.tick_params(axis='y', which='both', colors=AMAC_data_color) # both = major and minor

    # plot power supply current on same plot w/ different y-axis
    if(debug):
        print("VOLTAGE_vals_",VOLTAGE_vals_)
        print("PS_CURRENT_vals_",PS_CURRENT_vals_)
    PS_ax.plot(VOLTAGE_vals_, PS_CURRENT_vals_, color = PS_data_color, marker='o', markersize=2, linewidth=0.5)
    PS_ax.set_ylabel(r"PS current [${\mu}$A]", color=PS_data_color)
    PS_ax.spines['left'].set_color(AMAC_data_color)
    PS_ax.spines['right'].set_color(PS_data_color)
    PS_ax.yaxis.label.set_color(PS_data_color)
    PS_ax.tick_params(axis='y', which='both', colors=PS_data_color) # both = major and minor
    #ax.plot(x_vals_, y_vals_, linewidth=0.05) # very thin line

    PS_ax.spines['top'].set_color('black')
    ax.spines['top'].set_color('black')

    PS_ax.spines['bottom'].set_color('black')
    ax.spines['bottom'].set_color('black')


    # set ymax higher to contain plot labels
    ymin, ymax = ax.get_ylim()
    Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement
    ax.set_ylim(ymin, Incremented_Ymax)

    upperText = "%s"%(humanReadableName)

    if(type(TEMPERATURE_) == str): TEMPERATURE_ = TEMPERATURE_.replace("_", " ")
    
    plt.text(
        0.94,
        0.95,
        "\n".join([
            upperText,
            "Module IV curve",
            f"{TEMPERATURE_} \N{DEGREE SIGN}C"
        ]
        ),
        horizontalalignment='right',
        verticalalignment='top',
        fontweight = 'bold',
        transform=ax.transAxes
    )
    ax.set_xlim(xmin, xmax)
    
    PS_ax.ticklabel_format(style='plain')
    ax.ticklabel_format(style='plain')
    
    fig.tight_layout()
    Save_Figures(fig, fileTypes, OUTNAME_, "IVCurve")    

fileTypes = ["png", "pdf"]
print("in file:",inFile)
f = open(inFile)
data = json.load(f)
if(debug): print("data:",data)

component = data["component"]
TEMPERATURE = data["results"]["TEMPERATURE"]
if(debug):
    print("component:",component)
    print("TEMPERATURE:",TEMPERATURE)

dataset_names = [
            "CURRENT", "CURRENT_RMS", # from AMAC
            "PS_CURRENT", "VOLTAGE" # from PS
          ]

for dataset_name in dataset_names:
     exec("{}_vals = data['results']['{}']".format(dataset_name, dataset_name))
     if(debug):
         exec("print('{}_vals:',{}_vals)".format(dataset_name, dataset_name))

xlabel, ylabel = "Voltage [V]", "AMAC Current [nA]"
OUTNAME = "{}/{}_{}degC".format(ol, component, TEMPERATURE)
Make_IV_Plot(VOLTAGE_vals, CURRENT_vals, CURRENT_RMS_vals, PS_CURRENT_vals, xlabel, ylabel, OUTNAME, component, TEMPERATURE, debug) # include component as text on plot 
