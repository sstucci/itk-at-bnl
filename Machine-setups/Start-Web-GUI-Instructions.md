# Starting the coldjig s/w web gui 

The purpose of these instructions are to explain how to start the coldbox s/w GUI from scratch. This is used to monitor and control the various hardware components of the setup. 

1. If not already done, open a firefox window and a konsole terminal window by (double) clicking the icons on the desktop.
2. On the konsole terminal, type the command `ssh_pi` and press enter. You will then be asked for the password. If you do not know the password, ask Abe or Stefania. 
3. If the ssh into the pi was successful, you should see the terminal prompt start with `pi@raspberrypi:~ $`. If this is the case, you can switch to the software working directory by typing and entering the command `gui`. After this, type and enter these commands in order:

```
pipenv shell
source setenv.sh
./run.sh
```

If this was successful, you should begin to see some information printout in the window and you can move to step 4.

If there was already an attempted instance to start the coldjig s/w running before you began, at this stage you may receive the error:

```
OSError: [Errno 98] Address already in use
```

This is because there is a job already using the IP address provided in the configuration file to start an instance of the GUI. You can kill that already existing process in one way by running `ps -a` to list all running processes, and killing the python3 process which is likely the culprit by identifying its "PID" (process ID) from the `ps -a` printout, and running `kill -9 <PID>` to kill the process. After this, try `./run.sh` again to see if you can start the web gui. 

4. Open a firefox tab and click on the bookmark in the bookmarks bar of your desired webgui: `Coldjig-GUI-QC` or `Coldjig-GUI-Reception`. If this worked properly, you should see a window load the GUI and a Grafana dashboard. 
5. Start the GUI by clicking "Start" in the Control Panel tab. If this worked, you should see additional printout in the konsole where you ran `./run.sh`. If all is working well, the grafana panel should start to update. One can always set the time of the grafana values to be more recent to confirm - e.g. switching absolute time range from "Last 1 hour" to "Last 5 minutes" with the dropdown menu.
