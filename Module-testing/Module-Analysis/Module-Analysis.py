"""
28 December 2022
Abraham Tishelman-Charny

The purpose of this python module is to plot, compare, and analyze noise from ITk strips modules.

# Example usage:

# Plot ratio of files chosen explicitly

python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-012_box2/results/SCIPP-PPB_LS-012_RC_309_7.txt,/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-012_box3/results/SCIPP-PPB_LS-012_RC_28_7.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_012_Box2,SCIPP_PPB_LS_012_Box3,SCIPP_PPB_LS_012_Comparison" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/ --makeSeriesPlot --makeHist

python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-012_box2/results/SCIPP-PPB_LS-012_RC_309_7.txt,/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-012_box3/results/SCIPP-PPB_LS-012_RC_28_7.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_012_Box2,SCIPP_PPB_LS_012_Box3,SCIPP_PPB_LS_012_Comparison" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/ --makeSeriesPlot --makeHist
python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-016_box2/results/SCIPP-PPB_LS-016_RC_309_7.txt,/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-016_box3/results/SCIPP-PPB_LS-016_RC_28_7.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_016_Box2,SCIPP_PPB_LS_016_Box3,SCIPP_PPB_LS_016_Comparison" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/ --makeSeriesPlot --makeHist
python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-019_box2/results/SCIPP-PPB_LS-019_RC_309_7.txt,/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-019_box3/results/SCIPP-PPB_LS-019_RC_28_7.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_019_Box2,SCIPP_PPB_LS_019_Box3,SCIPP_PPB_LS_019_Comparison" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/ --makeSeriesPlot --makeHist
python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-023_box2/results/SCIPP-PPB_LS-023_RC_309_7.txt,/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-023_box3/results/SCIPP-PPB_LS-023_RC_28_7.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_023_Box2,SCIPP_PPB_LS_023_Box3,SCIPP_PPB_LS_023_Comparison" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/ --makeSeriesPlot --makeHist

# Other
python3 Module-Analysis.py --plotTogether --dataFromITSDAQ
python3 Module-Analysis.py --plotPerModule --dataFromCSV
python3 Module-Analysis.py --plotPerModule --dataFromITSDAQ --inDir /eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-012_box2/results/ --perSide --makeSeriesPlot --makeHist

"""

# imports 
import pandas as pd 
import numpy as np 
import os 
import csv 
import mplhep as hep 
from matplotlib import pyplot as plt 
plt.style.use(hep.style.ATLAS) # customization: https://mplhep.readthedocs.io/en/latest/api.html
from python.Module_Analysis_Tools import * 
from python.SetOptions import options 

# Command line flag options 
plotPerModule = options.plotPerModule
plotTogether = options.plotTogether
inDir = options.inDir 
ol = options.ol 
dataFromITSDAQ = options.dataFromITSDAQ
dataFromCSV = options.dataFromCSV
makeSeriesPlot = options.makeSeriesPlot
makeHist = options.makeHist
perSide = options.perSide
NoInteractiveMode = options.NoInteractiveMode
plotRatio = options.plotRatio
ratioFiles = options.ratioFiles.split(',')
ratioFilesLabels = options.ratioFilesLabels.split(',')

if(NoInteractiveMode): 
    import matplotlib
    matplotlib.use('Agg') # turn off interactive mode

if(dataFromITSDAQ + dataFromCSV != 1):
   raise Exception("Need to choose either dataFromITSDAQ or dataFromCSV")

# Create output directory if it doesn't already exist
if(not os.path.exists(ol)):
    print("Creating output directory:",ol)
    os.system("mkdir -p %s"%(ol))
    os.system("cp %s/../index.php %s"%(ol, ol)) # assumes one directory up from 'ol' exists

# Functions 
def Save_Figures(fig, fileTypes_, OUTNAME_, plotType):
    for fileType in fileTypes_:
        outname = "%s_%s.%s"%(OUTNAME_, plotType, fileType)
        print("")
        print("Saving figure:",outname)
        print("")
        fig.savefig(outname)
    plt.close()    

def Make_Plots(x_vals_, x_label, y_vals_, y_vals_err_, OUTNAME_, Module_, Value_type_, SET_NUMBER_, add_side_number, makeSeriesPlot, makeHist):
    unit_dict = {
        "Current" : "$\mu$A",
        "Voltage" : "mV",
        "Gain" : "",
        "vt50" : "",
        "Input_noise" : r"$e^{-}$"
    }

    yunit = unit_dict[Value_type_]
    xmin, xmax = min(x_vals_), max(x_vals_)

    # params 
    fileTypes = ["png", "pdf"]
    Nentries = len(y_vals_)
    ymaxIncrement = 0.333
    quantity_nonlfn_label = Value_type_.replace("_", " ")
    if(yunit == ""): y_label = quantity_nonlfn_label # if unitless, no brackets
    else: y_label = r"%s [%s]"%(quantity_nonlfn_label, yunit)

    if(makeSeriesPlot):
        # series (values vs x quantity)
        fig, ax = plt.subplots(1, figsize=(6, 4), dpi=100)
        
        if(y_vals_err_ is not None):
            ax.errorbar(x_vals_, y_vals_, yerr=y_vals_err_, xerr=None, marker='o', markersize=2, elinewidth=0.5, linewidth=0.5)
        #else: ax.plot(x_vals_, y_vals_)
        else: ax.plot(x_vals_, y_vals_, linewidth=0.05) # very thin line
        hep.atlas.text("ITk Strips Internal")
        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)

        # set ymax higher to contain plot labels
        ymin, ymax = ax.get_ylim()
        Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement
        ax.set_ylim(ymin, Incremented_Ymax)

        # Upper right text 
        if(add_side_number): upperText = "%s, side %s"%(Module_,SET_NUMBER_)
        else: upperText = "%s"%(Module_)
        plt.text(
            0.94,
            0.95,
            "\n".join([
                upperText,
                quantity_nonlfn_label
            ]
            ),
            horizontalalignment='right',
            verticalalignment='top',
            fontweight = 'bold',
            transform=ax.transAxes
        )
        ax.set_xlim(xmin, xmax)
        fig.tight_layout()
        Save_Figures(fig, fileTypes, OUTNAME_, "series")

    if(makeHist):
        # histogram (values integrated over time)
        fig, ax = plt.subplots(1, figsize=(6, 4), dpi=100)
        counts, bins = np.histogram(y_vals_)
        avg, stdev = np.mean(y_vals_), np.std(y_vals_)
        ax.hist(bins[:-1], bins, weights=counts)
        hep.atlas.text("ITk Strips Internal")    
        ax.set_xlabel(y_label)
        ax.set_ylabel("Entries")

        # set ymax higher to contain plot labels
        ymin, ymax = ax.get_ylim()
        Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement
        ax.set_ylim(ymin, Incremented_Ymax)

        # Upper left text 
        plt.text(
            0.06,
            0.95,
            "\n".join([
                "$\mu$ = %s" % float('%.4g' % avg),
                "$\sigma$ = %s" % float('%.4g' % stdev),
                "$N_{entries}$ = %s"%(Nentries)
            ]
            ),
            horizontalalignment='left',
            verticalalignment='top',
            fontweight = 'bold',
            transform=ax.transAxes
        )

        # Upper right text 
        plt.text(
            0.94, 
            0.95,
            "\n".join([
                upperText,
                quantity_nonlfn_label
            ]
            ),
            horizontalalignment='right',
            verticalalignment='top',
            fontweight = 'bold',
            transform=ax.transAxes
        )
        fig.tight_layout()
        Save_Figures(fig, fileTypes, OUTNAME_, "history")

        # Save avg and stdev 
        csv_outname = '%s_stats.csv'%(OUTNAME_)
        print("Saving csv file:",csv_outname)
        row = [avg, stdev]
        f = open(csv_outname,'w')
        writer = csv.writer(f)
        writer.writerow(row)
        f.close()

def Make_Combined_Plot(x_label, all_module_vals_, y_errs_, x_vals_, modules_, quantity_, side_, OUTNAME_, perSide_, plotLog_):
    
    unit_dict = {
        "Current" : "$\mu$A",
        "Voltage" : "mV",
        "Gain" : "",
        "vt50" : "",
        "Input_noise" : r"$e^{-}$"
    }

    yunit = unit_dict[quantity_]

    # parameters 
    fileTypes = ["png", "pdf"]
    N_modules = len(all_module_vals_)
    xmin, xmax = min(x_vals_), max(x_vals_)
    ymaxIncrement = 0.6
    quantity_nonlfn_label = quantity_.replace("_", " ")

    # make ratio plot 
    fig, ax = plt.subplots(2, figsize=(6, 4), sharex=True, dpi=100, gridspec_kw={'height_ratios': [3, 1]})

    lower = ax[1]
    upper = ax[0]

    if(yunit == ""): y_label = quantity_nonlfn_label # if unitless, no brackets
    else: y_label = r"%s [%s]"%(quantity_nonlfn_label, yunit)

    # to get local IDs correct without having to worry about dashes not allowable as tokens for names
    Module_Label_Dict = {
        "SCIPP_PPB_LS_012_Box2" : "SCIPP-PPB_LS-012_Box2",
        "SCIPP_PPB_LS_012_Box3" : "SCIPP-PPB_LS-012_Box3",

        "SCIPP_PPB_LS_016_Box2" : "SCIPP-PPB_LS-016_Box2",
        "SCIPP_PPB_LS_016_Box3" : "SCIPP-PPB_LS-016_Box3",

        "SCIPP_PPB_LS_019_Box2" : "SCIPP-PPB_LS-019_Box2",
        "SCIPP_PPB_LS_019_Box3" : "SCIPP-PPB_LS-019_Box3",

        "SCIPP_PPB_LS_023_Box2" : "SCIPP-PPB_LS-023_Box2",
        "SCIPP_PPB_LS_023_Box3" : "SCIPP-PPB_LS-023_Box3",                        
    }

    for m_i, module_vals in enumerate(all_module_vals_):

        
        if(modules_[m_i] in Module_Label_Dict): module_label = Module_Label_Dict[modules_[m_i]]
        else: module_label = modules_[m_i]


        if(y_errs_ is not None):
            #ax.errorbar(x_vals_, y_vals_, yerr=y_errs_[m_i], xerr=None, marker='o', markersize=4, elinewidth=2, linewidth=1)
            upper.errorbar(x_vals_, module_vals, yerr=y_errs_[m_i], linestyle="-", marker="o", label = module_label, linewidth=0.25, markersize=0.2)
            
        else: 
            upper.plot(x_vals_, module_vals, 'o-', label = module_label, linewidth=0.25, markersize=0.25)

        #upper.plot(x_vals_, module_vals, 'o-', label = modules_[m_i], linewidth=0.25, markersize=0.25)
        ratio_vals = np.divide(module_vals, all_module_vals_[0])
        lower.plot(x_vals_, ratio_vals, 'o', linewidth=0, markersize=0.25)
    
    lower.set_xlabel(x_label)
    upper.set_ylabel(y_label)    

    upper.set_xlim(xmin, xmax)
    lower.set_xlim(xmin, xmax)

    lower.axhline(y=1, color='black', linestyle='dashed', linewidth=0.25)

    firstModuleName = modules_[0]

    lowerYlabel = "\n".join([
        "Ratio to %s"%(firstModuleName.split("_")[-1]),
    ])

    # good for modules 
    # lowerYlabel = "\n".join([
    #     "Ratio to         ",
    #     firstModuleName
    # ])

    lower.set_ylabel(lowerYlabel, fontsize=10)

    #for plotLog in [0,1]:

    if(plotLog_):
        OUTNAME_full = "%s_log"%(OUTNAME_)
        ymaxIncrement_Total = ymaxIncrement*10.
    else:
        OUTNAME_full = OUTNAME_
        ymaxIncrement_Total = ymaxIncrement

    # set ymax higher to contain plot labels
    ymin, ymax = upper.get_ylim()
    Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement_Total
    if(plotLog_):
        upper.set_yscale('log')        
    upper.set_ylim(ymin, Incremented_Ymax)    
    #upper.legend(fontsize=7.5, loc = 'upper center')
    upper.legend(fontsize=12, loc = 'upper left')
    #if(not plotLog): 
        #print("No scientific notation")
        #upper.ticklabel_format(style='plain') # remove scientific notation

    hep.atlas.text("ITk Strips Internal", ax=upper)
    fig.tight_layout()

    if(perSide_):
        # Stream number 
        plt.text(
            0.94,
            0.95,
            "\n".join([
                "Stream %s"%(int(side_)-1)
            ]
            ),
            horizontalalignment='right',
            verticalalignment='top',
            fontweight = 'bold',
            transform=upper.transAxes
        )    
    Save_Figures(fig, fileTypes, OUTNAME_full, "series")    

# go through modules just to get values into lists/arrays
for module_i, module in enumerate(modules): # assumes data is in form of txt files output from ITSDAQ
    print("On module:",module)
    module_str = module.replace("-","_")

    # data from standard ITSDAQ output files 
    if(dataFromITSDAQ):
        inFile = "%s/%s_%s_%s_%s.txt"%(inDir, module, RESULT_TYPE, RUN_NUMBER, SCAN_NUMBER)
        df = pd.read_csv(inFile, delimiter="\t")
        print("df:",df)

        # for each quantity, make a plot for the module
        Nstrips = 1280 # per side
        #Nstrips = 394240 # hack for huge input files which combine a lot of files
        for quantity in quantities:
            print("On quantity:",quantity)
            vals = np.array(df[quantity].tolist())
            channels = np.array(df["#chan"].tolist())

            if(perSide):
                strips_1_vals = vals[:Nstrips] # values for first set of strips
                strips_2_vals = vals[Nstrips:] # values for second set of strips 
                strips_1_channels = channels[:Nstrips]
                strips_2_channels = channels[Nstrips:]
            else: # just set them to the same thing
                strips_1_vals = vals[:] # values for first set of strips
                strips_2_vals = vals[:] # values for second set of strips 
                strips_1_channels = channels[:]
                strips_2_channels = channels[:]                

            # save values for later in case you want to make a combined plot 
            exec("%s_%s_1_y_vals = np.copy(strips_1_vals)"%(module_str, quantity)) # module, quantity, side 
            exec("%s_%s_2_y_vals = np.copy(strips_2_vals)"%(module_str, quantity)) # module, quantity, side     
            
            strips_1_channels = np.copy(strips_1_channels)
            strips_2_channels = np.copy(strips_2_channels)           

            quant_label = quantity_dict[quantity]

            if(plotPerModule):
                if(perSide):
                    for SET_NUMBER in ["1", "2"]:
                        # make two types of plots: distribution of values, and value vs. channel number 
                        OUTNAME = '%s/%s_%s_%s'%(ol, module, quant_label, SET_NUMBER)
                        exec("These_channels = strips_%s_channels"%(SET_NUMBER))
                        exec("These_vals = strips_%s_vals"%(SET_NUMBER))
                        vals_rms = None
                        x_label = "Channel number"
                        add_side_num = 1
                        Make_Plots(These_channels, x_label, These_vals, vals_rms, OUTNAME, module, quant_label, SET_NUMBER, add_side_num, makeSeriesPlot, makeHist)
                else:
                    # One plot, combine sides
                    SET_NUMBER = 1
                    OUTNAME = '%s/%s_%s_%s'%(ol, module, quant_label, SET_NUMBER)
                    exec("These_channels = strips_%s_channels"%(SET_NUMBER))
                    exec("These_vals = strips_%s_vals"%(SET_NUMBER))
                    vals_rms = None
                    #x_label = "Channel number"
                    x_label = r"$Channel_{N}{\times}Measurement_{N}$"
                    add_side_num = 0
                    These_channels = [i for i in range(Nstrips)]
                    Make_Plots(These_channels, x_label, These_vals, vals_rms, OUTNAME, module, quant_label, SET_NUMBER, add_side_num, makeSeriesPlot, makeHist)


    # data from custom CSV file
    elif(dataFromCSV):
        inFile = "%s/%s_%s.csv"%(inDir, module, RESULT_TYPE)
        df = pd.read_csv(inFile, delimiter=",", header=None)
        print("df",df)
        quant_column_dict = { # columns with quantity and its RMS
            "Current" : [2, 3],
            "Voltage" : [4, 5]
        }
        # for each quantity, make a plot for the module
        for quantity in quantities:
            print("On quantity:",quantity)
            val_col, val_rms_col = quant_column_dict[quantity]

            vals = np.array(df[val_col].tolist())
            vals_rms = np.array(df[val_rms_col].tolist())

            print("Before:")

            if(quantity == "Current"): 
                vals = np.array([i/1000. for i in vals]) # convert nA to muA.
                vals_rms = np.array([i/1000. for i in vals]) # convert nA to muA.

            quant_label = quantity_dict[quantity]
            # make two types of plots: distribution of values, and value vs. time (x axis value time now not channel number)
            times = np.array(df[0].tolist())
            initial_time = times[0] 
            times = np.array([time-initial_time for time in times])

            # save values in case you want to plot them later
            exec("%s_%s_y_vals = np.copy(vals)"%(module_str, quantity))
            exec("%s_%s_y_vals_rms = np.copy(vals_rms)"%(module_str, quantity))
            exec("%s_%s_x_vals = np.copy(times)"%(module_str, quantity))

            if(plotPerModule):
                OUTNAME = "%s/%s_%s"%(ol, module, quant_label)
                SET_NUMBER = "SET_NUMBER_BLANK"
                x_label = "time [s]"
                add_side_num = 0
                Make_Plots(times, x_label, vals, vals_rms, OUTNAME, module, quant_label, SET_NUMBER, add_side_num, makeSeriesPlot, makeHist)

if(plotTogether):
    if(perSide): 
        sides = ["1", "2"]
        OUTNAME = '%s/%s_%s_%s'%(ol, "AllModules", quant_label, side) # wrong? side number here?
    else: 
        sides = ["NoSides"]    
        OUTNAME = '%s/%s_%s'%(ol, "AllModules", quant_label)    
    for quantity in quantities:
        print("On quantity:",quantity)
        for side in sides:
            print("On side:",side)
            all_module_vals = []
            all_module_vals_rms = []
            for module in modules:
                module_str = module.replace("-","_")
                if(dataFromITSDAQ):
                    x_label = "Channel number"
                    exec("these_y_vals = %s_%s_%s_y_vals"%(module_str, quantity, side))
                    these_y_vals_rms = None
                    exec("these_x_vals = strips_%s_channels"%(side)) # taking most recent channel vals. assuming same for all modules
                elif(dataFromCSV):
                    x_label = "time [s]"
                    exec("these_y_vals = %s_%s_y_vals"%(module_str, quantity))
                    exec("these_y_vals_rms = %s_%s_y_vals_rms"%(module_str, quantity))
                    exec("these_x_vals = %s_%s_x_vals"%(module_str, quantity))
                all_module_vals.append(these_y_vals)
                all_module_vals_rms.append(these_y_vals_rms)
            quant_label = quantity_dict[quantity]
            if(dataFromITSDAQ): 
                all_module_vals_rms = None

            OUTNAME = '%s/%s_%s_%s'%(ol, "AllModules", quant_label, side)
            for plotLog in [0,1]:
                Make_Combined_Plot(x_label, all_module_vals, all_module_vals_rms, these_x_vals, modules, quant_label, side, OUTNAME, perSide, plotLog)

# for just choosing files by hand
if(plotRatio):

    # Structure values into lists
    for file_i, file in enumerate(ratioFiles):
        fileLabel = ratioFilesLabels[file_i]
        df = pd.read_csv(file, delimiter="\t")
        #print("df:",df)
        Nstrips = 1280 # per side
        for quantity in quantities:
            print("On quantity:",quantity)

            vals = np.array(df[quantity].tolist())
            channels = np.array(df["#chan"].tolist())

            if(perSide):
                strips_1_vals = vals[:Nstrips] # values for first set of strips
                strips_2_vals = vals[Nstrips:] # values for second set of strips 
                strips_1_channels = channels[:Nstrips]
                strips_2_channels = channels[Nstrips:]
            else: # just set them to the same thing
                strips_1_vals = vals[:] # values for first set of strips
                strips_2_vals = vals[:] # values for second set of strips 
                strips_1_channels = channels[:]
                strips_2_channels = channels[:]                

            # save values for later in case you want to make a combined plot 
            exec("%s_%s_1_y_vals = np.copy(strips_1_vals)"%(fileLabel, quantity)) # fileLabel, quantity, side 
            exec("%s_%s_2_y_vals = np.copy(strips_2_vals)"%(fileLabel, quantity)) # fileLabel, quantity, side     
        
            strips_1_channels = np.copy(strips_1_channels)
            strips_2_channels = np.copy(strips_2_channels)           

            quant_label = quantity_dict[quantity]

    if(perSide): 
        sides = ["1", "2"]
    else: 
        sides = ["NoSides"]    
    
    for quantity in quantities:
        print("On quantity:",quantity)
        for side in sides:
            print("On side:",side)
            all_module_vals = []
            all_module_vals_rms = []

            for file_i, file in enumerate(ratioFiles):
                fileLabel = ratioFilesLabels[file_i]                
                print("fileLabel:",fileLabel)
                if(dataFromITSDAQ):
                    x_label = "Channel number"
                    exec("these_y_vals = %s_%s_%s_y_vals"%(fileLabel, quantity, side))
                    these_y_vals_rms = None
                    exec("these_x_vals = strips_%s_channels"%(side)) # taking most recent channel vals. assuming same for all modules
                elif(dataFromCSV):
                    x_label = "time [s]"
                    exec("these_y_vals = %s_%s_y_vals"%(fileLabel, quantity))
                    exec("these_y_vals_rms = %s_%s_y_vals_rms"%(fileLabel, quantity))
                    exec("these_x_vals = %s_%s_x_vals"%(fileLabel, quantity))
                all_module_vals.append(these_y_vals)
                all_module_vals_rms.append(these_y_vals_rms)
            
            quant_label = quantity_dict[quantity]
            if(dataFromITSDAQ): 
                all_module_vals_rms = None

            Ratio_Label = ratioFilesLabels[-1] # reserve final label for name of ratio plot

            OUTNAME = '%s/%s_%s_%s'%(ol, Ratio_Label, quant_label, side)
            for plotLog in [0]:
                Make_Combined_Plot(x_label, all_module_vals, all_module_vals_rms, these_x_vals, ratioFilesLabels, quant_label, side, OUTNAME, perSide, plotLog)