#ifndef TkHV_h
#define TkHV_h

//
//  TkHV.h
//
//  CGA 12 June 2012
//  PWP 26 May 2010
//

#define PROTECT_MAX_10V_PER_STEP false


#if _MSC_VER >= 1000
#pragma once
#endif
#if defined _MSC_VER && !defined ROOT_w32pragma
#include <w32pragma.h>
#endif
#include "TSystem.h"
#include "TObject.h"
#include "TGraphErrors.h"

#ifdef _MSC_VER
// Must be something more minimal for types
#include "windows.h"
#else
#include "math.h"
#include "stdlib.h"
#endif
#include <memory> // std::unique_ptr
#include "../dcsdll/TDCSDevice.h"

#define MAX_IV_POINTS 512
#define kHV_TIMEOUT_SHORT 500 //mS
#define kHV_TIMEOUT_LONG 5000 //mS
#define kHV_DELAY_SHORT 0.0 //S
#define kHV_DELAY_LONG  1.0 //S

class TkHVPrivate;

class TkHV : public TObject, public TDCSDevice {
  constexpr static Int_t DEFAULT_TIMEOUT = 500; // ms

 public:
  enum class DeviceType {
    SCPI_COMPLIANT,
    LEGACY,
    ITKHV,
    MAX, // for range checking
  };
  TkHV(const char* resName, bool SCPI = true);
  TkHV(const char* resName, DeviceType deviceType);
  TkHV(const char* resName, const char* label, bool SCPI = true);
  TkHV(const char* resName, const char* label, DeviceType deviceType);
  TkHV(const char* resName, int output, const char* label, DeviceType deviceType);
  ~TkHV();

  void Abort();
  Bool_t Busy();

  TGraph* GetIVGraph(int channel);
  Bool_t HitCompliance();

  Bool_t Init();
  Bool_t IVScan(Float_t vStart, Float_t vStop, Float_t vStep, Float_t vEnd, Float_t iLimit, Int_t msDelay, Bool_t append = false);
  Bool_t IVScanWithPB(Float_t vStart, Float_t vStop, Float_t vStep, Float_t vEnd, Float_t iLimit, Int_t msDelay, Bool_t append, int nPB);
  void   IVScanBG(Float_t vStart, Float_t vStop, Float_t vStep, Float_t vEnd, Float_t iLimit, Int_t msDelay, Bool_t append = false);
  Bool_t IVScanImpl(Float_t vStart, Float_t vStop, Float_t vStep, Float_t vEnd, Float_t iLimit, Int_t msDelay, Bool_t append, int nPowerboards);
  Float_t CalcPBCompliance(Float_t voltage, Float_t iLimit, int nPB);

  void Log(FILE* outfile);
  void LogIV(FILE* outfile);

  Bool_t Mon();
  Bool_t Mon(unsigned char* status, Float_t* V, Float_t* I);

  Bool_t Off();
  Bool_t On();

  Bool_t PrintStatus();

  // NB Defaults set in TDCSDevice.h
  Bool_t Ramp(Float_t vStop, Float_t iLimit, int rampRate);
  void   RampBG(Float_t vStop, Float_t iLimit = 10, int rampRate = 3);
  Bool_t RampImpl(Float_t vStop, Float_t iLimit = 10, int rampRate = 3);
	Bool_t RampDown(int speed = 3);
  Bool_t RampUp(int V, int speed = 3);

  Bool_t ReadLabel(char* label) const;
  Bool_t ReadLimits(Float_t* V, Float_t* I);
  Bool_t ReadSettings();
  Bool_t ReadSettings(Float_t* V, Float_t* I);

  Bool_t Set(Float_t v, Float_t i);
  Bool_t SetDelay(Float_t delay);
  Bool_t SetFilter(int nSamples);
  Bool_t SetLimit(Float_t V);
  Float_t GetLimit();
  Bool_t SetTerminals(bool rear);

  std::string GetModel() const;
  std::string GetResourceString() const;
  std::string GetSerialNumber() const;

  Bool_t ClearErrors();

  static std::string deviceTypeName(DeviceType dt);

  /// Set global serial debug flag
  static void SetDebug(bool d);

 private:
  void Init(const char* resName, const char* label, bool SCPI);
  void Init(const char* resName, const char* label, DeviceType deviceType);
  void Init(const char* resName, int output, const char* label, DeviceType deviceType);

  /**
   * @brief Send a command to the device.
   * @param command  Command to send (including line ending)
   * @param where    Location in program for error/debug output
   * @return @c true if successful, @c false otherwise.
   */
  bool sendCommand(const std::string& command, const std::string &where);

  /**
   * @brief Send sequence of commands to the device.
   * @param command  Command to send (including line ending)
   * @param where    Location in program for error/debug output
   * @return @c true if successful, @c false otherwise.
   */
  bool sendCommands(const std::vector<std::string> commands, const std::string &where) {
    for(auto c: commands) {
      if (! sendCommand(c, where)) return false;
    }
    return true;
  }

  /// Flush output, mainly for ITKHV (which is more verbose)
  void FlushDeviceBuffer(Int_t timeout = DEFAULT_TIMEOUT);

  /**
   * @brief Send a command and read a number in response
   * @param[in] input  The command to send to the instrument.
   * @param[out] num   A reference to the variable in which the result will be stored.
   * @param[in] scale  A constant to multiply the result by.
   * @return @c true if successful, @c false otherwise.
   *
   * Use as @c readNum("v\n", vMon) etc.
   * As the underlying string-to-number conversion function is @c atof(),
   * this should be a float, but stick to double for compatibility.
   */
  bool readNum(const char* const input, double& num, int scale = 1);

  Double_t vMax, iMax, vMon, iMon, vSafety, vSet, iSet;

  Double_t isegVMon[4];
  Double_t isegIMon[4];


  enum class DeviceState {
    OFF = 0,
    ON = 1,
    TRIPPED = 2,
  } state; // replaces Bool_t on; because ITKHV has three states
  
  unsigned char data[1024];
  char stringinput[256];
  char m_resource[128];
  bool outputBool;
  char outLabel[64];

  char sModel[20];
  int  iModel;
  int nChannels;
  Float_t fDelay;

  Bool_t do_abort;
  Bool_t do_threaded_ramp;
  Bool_t do_threaded_scan;

  int hd; // thread handle

  Double_t xPoints[10][MAX_IV_POINTS], yPoints[10][MAX_IV_POINTS];
  Int_t nPoints;

  TGraph *ivGraph;
  friend class BindRamp;
  friend class BindScan;
  friend bool ItkHvRamp(float vStop, float iLimit, int rampRate, TkHV &p);

  // The comment on the following line suppresses an incomplete type warning.
  std::unique_ptr<TkHVPrivate> port; //!

  ClassDef(TkHV,1)
};
#endif
