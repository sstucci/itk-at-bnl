// These are needed for ROOT6, but skip over during CI
#if !defined(ITSDAQ_BUILD_CHECK)
#include "stdll/TST.h"
#include "stdll/TModule.h"

#include "dcsdll/TDCSDevice.h"
#else
#include "runtimeCommon.h"
#include "ttidll/TTi.h"
#include "khvdll/TkHV.h"
#endif

void load_AFG(Int_t pulsertype){

  std::cout << "Loading AFG" << std::endl;

  char pulserName[20] = "GPIB0::1::INSTR";
  AFG = new TVgen(pulserName,1);
}

void load_LV_supplies(Int_t lvtype){
  /*
   * Loads the correct LV supply infrastructure 
   * Makes use of global variables LVSupplies and nLVSupplies
   * Args:
   *  lvtype: int which configures the LV supplies. Should be 0, 1, 2, 3, 4
   *          0 inidcates manual control of LV
   *          4 indicates ITSDAQ GUI control of LV
   */

  std::cout << "Loading LV supplies (" << lvtype << ")\n";

  switch(lvtype){
    case 1: // FTDI driver for Custom Current Source
      // WJF disable for ROOT6 (not needed)
      exit(1);

    case 2: // NI-VISA driver for TTi TSX3510P or TSX1820P
      // WJF disable for ROOT6 
      exit(1);

    case 4:
      {
        // create new TTi instance, put into vector 
        // WJF: why ASRL5::INSTR. Use "/usr/local/bin/lsni -a" to display resources detected by NI system 
        // ASRL5::INSTR is the one that appears and then dissapears when swithcing on/off the power supply!

        char psName[20] = "ASRL5::INSTR";
        char modName[20] = "Module 0";

        // Indirection so we can still link if the library is not loaded
        LVSupplies[nLVSupplies] = new TTi(psName, 1, modName);

        // check if valid
        if(LVSupplies[nLVSupplies]->isValid){
          nLVSupplies++;
          printf("INFO: LV supply is valid\n");
          std::cout << "INFO: There are " << nLVSupplies << " LV supplies" << std::endl;
        }else{
          printf("WARNING: LV supply is not valid!");
        }

        // intentional fall through

        break;
      }

    case 130:
      {
        // create new TTi instance, put into vector 
        // WJF: why ASRL5::INSTR. Use "/usr/local/bin/lsni -a" to display resources detected by NI system 
        // ASRL5::INSTR is the one that appears and then dissapears when swithcing on/off the power supply!

        char psName[20] = "ASRL4::INSTR";
        char modName1[20] = "VDDD";
        char modName2[20] = "VDDA";

        // Indirection so we can still link if the library is not loaded
        LVSupplies[nLVSupplies] = new TTi(psName, 1, modName1);

        // check if valid
        if(LVSupplies[nLVSupplies]->isValid){
          std::cout << "INFO: LV Supply " << nLVSupplies << " is valid" << std::endl;
          nLVSupplies++;
        }else{
          LVSupplies[nLVSupplies] = 0;
          std::cout << "WARNING: LV supply is not valid!" << std::endl; 
        }


        // Indirection so we can still link if the library is not loaded
        LVSupplies[nLVSupplies] = new TTi(psName, 2, modName2);

        // check if valid
        if(LVSupplies[nLVSupplies]->isValid){
          std::cout << "INFO: LV Supply " << nLVSupplies << " is valid" << std::endl;
          nLVSupplies++;
        }else{
          LVSupplies[nLVSupplies] = 0;
          std::cout << "WARNING: LV supply is not valid!" << std::endl; 
        }

        char psName3[20] = "ASRL3::INSTR";
        char modName3[20] = "NEXYS";

        // Indirection so we can still link if the library is not loaded
        LVSupplies[nLVSupplies] = new TTi(psName3, 2, modName3);

        // check if valid
        if(LVSupplies[nLVSupplies]->isValid){
          std::cout << "INFO: LV Supply " << nLVSupplies << " is valid" << std::endl;
          nLVSupplies++;
        }else{
          LVSupplies[nLVSupplies] = 0;
          std::cout << "WARNING: LV supply is not valid!" << std::endl; 
        }
        break;
      }

    case -1:
      {
        std::string file_name = Form("%spower_supplies.json", getConfigDirectory());
        std::cout << "Check json file: " << file_name << "\n";
        Json *j = new Json(file_name);

        std::string lv_name = "/lv_supplies";

        if(!j->IsValid(lv_name)) {
          std::cout << "No JSON file\n";
          return;
        }

        std::cout << "Loading supplies from JSON file\n";

        int i=0;
        while(1) {
          std::string aname = Form("%s/%d", lv_name.c_str(), i);
          if(!j->IsValid(aname.c_str())) {
            break;
          }

          std::string psName = j->ReadString(Form("%s/psName", aname.c_str()));
          std::string modName = j->ReadString(Form("%s/numChannels", aname.c_str()));
          char *psString = new char[psName.size() + 1];
          char *modString = new char[modName.size() + 1];
          strcpy(psString, psName.c_str());
          strcpy(modString, modName.c_str());

          int channel = 1;
          if(j->IsValid(Form("%s/channel", aname.c_str()))) {
            channel = j->ReadInteger(Form("%s/channel", aname.c_str()));
          }

          std::cout << "Create TTi from json:\n";
          std::cout << " " << psName << " for " << modName << "\n";

          LVSupplies[nLVSupplies] = new TTi(psString, channel, modString);

          delete [] psString;
          delete [] modString;

          // check if valid
          if(LVSupplies[nLVSupplies]->isValid) {
            nLVSupplies++;
            std::cout << "INFO: LV supply is valid, there are now " << nLVSupplies << " LV supplies\n";
          } else {
            std::cout << "WARNING: LV supply is not valid (count remains at " << nLVSupplies << ")!";
          }

          i++;
        } // End of while over json array

        break;
      }

    case 333:
      {
        // create new TTi instance, put into vector 
        // WJF: why ASRL5::INSTR. Use "/usr/local/bin/lsni -a" to display resources detected by NI system 
        // ASRL5::INSTR is the one that appears and then dissapears when swithcing on/off the power supply!

        //char psName[20] = "CPX400DP100";
        char psName1[40] = "TCPIP0::192.168.222.100::9221::SOCKET";
        char psName2[40] = "TCPIP0::192.168.222.110::9221::SOCKET";
        char modName1[20] = "MASTER";
        char modName2[20] = "SLAVE";
        char modName3[20] = "1V5";

        // Indirection so we can still link if the library is not loaded
        LVSupplies[nLVSupplies] = new TTi(psName1, 1, modName1);

        // check if valid
        if(LVSupplies[nLVSupplies]->isValid){
          std::cout << "INFO: LV Supply " << nLVSupplies << " is valid" << std::endl;
          nLVSupplies++;
        }else{
          LVSupplies[nLVSupplies] = 0;
          std::cout << "WARNING: LV supply is not valid!" << std::endl; 
        }


        // Indirection so we can still link if the library is not loaded
        LVSupplies[nLVSupplies] = new TTi(psName1, 2, modName2);

        // check if valid
        if(LVSupplies[nLVSupplies]->isValid){
          std::cout << "INFO: LV Supply " << nLVSupplies << " is valid" << std::endl;
          nLVSupplies++;
        }else{
          LVSupplies[nLVSupplies] = 0;
          std::cout << "WARNING: LV supply is not valid!" << std::endl; 
        }

        // Indirection so we can still link if the library is not loaded
        LVSupplies[nLVSupplies] = new TTi(psName2, 2, modName3);

        // check if valid
        if(LVSupplies[nLVSupplies]->isValid){
          std::cout << "INFO: LV Supply " << nLVSupplies << " is valid" << std::endl;
          nLVSupplies++;
        }else{
          LVSupplies[nLVSupplies] = 0;
          std::cout << "WARNING: LV supply is not valid!" << std::endl; 
        }
        break;
      }
    
      default:
        {
          if(nLVSupplies > 1){
            std::cout << "ERROR: should not have so many LV supplies for manual control!" << std::endl;
            exit(1);
          }
          else{
            LVSupplies[0]=0;
          }
          break;
        }

  }
}

void load_HV_supplies(int hvtype) { 
  std::cout << "Loading HV supplies with hvtype: " <<  hvtype << std::endl;

  // Edit the following section to match your  HV power setup
  std::cout << "hvtype: " << hvtype << std::endl;
  switch(hvtype){
  case 1: // NI-VISA driver for Keithley 237, 487 or 2410

    //HVSupplies[nHVSupplies] = new TkHV("GPIB0::22::INSTR","Module 0");
    //if(HVSupplies[nHVSupplies]->isValid) nHVSupplies++;
    //else HVSupplies[nHVSupplies] = 0;
    break;

  case 2:
    //HVSupplies[nHVSupplies] = new TkHV("ASRL6::INSTR", "Module 0");
    //if (HVSupplies[nHVSupplies]->isValid) nHVSupplies++;
    //else HVSupplies[nHVSupplies] = 0;
    break;

  case -1:
    {
      std::string file_name = Form("%spower_supplies.json", getConfigDirectory());
      std::cout << "Check json file: " << file_name << "\n";
      Json *j = new Json(file_name);

      std::string hv_name = "/hv_supplies";

      if(!j->IsValid(hv_name)) {
        std::cout << "No JSON file\n";
        return;
      }

      std::cout << "Loading HV supplies from JSON file\n";

      int i=0;
      while(1) {
        std::string aname = Form("%s/%d", hv_name.c_str(), i);
        if(!j->IsValid(aname.c_str())) {
          break;
        }

        std::string psName = j->ReadString(Form("%s/psName", aname.c_str()));
        std::string modName = j->ReadString(Form("%s/numChannels", aname.c_str()));
        using DeviceType = typename TkHV::DeviceType;
        DeviceType deviceType = DeviceType::SCPI_COMPLIANT;
        std::string deviceTypeAddress = Form("%s/deviceType", aname.c_str());
        if (j->IsValid(deviceTypeAddress)) {
         try {
          int iDeviceType = j->ReadInteger(deviceTypeAddress);
          if (iDeviceType >= 0 && iDeviceType < static_cast<int>(DeviceType::MAX)) {
            deviceType = static_cast<DeviceType>(iDeviceType);
          }
         } catch(std::exception &e) {
            auto dt = j->ReadString(deviceTypeAddress);
            if(dt == "SCPI" || dt == "2410") {
              deviceType = DeviceType::SCPI_COMPLIANT;
            } else if(dt == "LEGACY") {
              deviceType = DeviceType::LEGACY;
            } else if(dt == "ITKHV") {
              deviceType = DeviceType::ITKHV;
            } else {
              std::cout << "Device type not recognised, allowed:\n"
                        << "SCPI, 2410, LEGACY, ITKHV\n";
            }
         }
        }
        char *psString = new char[psName.size() + 1];
        char *modString = new char[modName.size() + 1];
        strcpy(psString, psName.c_str());
        strcpy(modString, modName.c_str());

        int output = 1;
        if(j->IsValid(Form("%s/output", aname.c_str()))) {
          output = j->ReadInteger(Form("%s/output", aname.c_str()));
        }

        std::cout << "Create TkHV from json:\n";
        std::cout << " " << psName << " for " << modName << ", deviceType = " << static_cast<int>(deviceType) << " (" << TkHV::deviceTypeName(deviceType) << ")\n";

        HVSupplies[nHVSupplies] = new TkHV(psString, output, modString, deviceType);

        delete [] psString;
        delete [] modString;

        // check if valid
        if(HVSupplies[nHVSupplies]->isValid) {
          nHVSupplies++;
          std::cout << "INFO: HV supply is valid, there are now " << nHVSupplies << " HV supplies\n";
        } else {
          std::cout << "WARNING: HV supply is not valid (count remains at " << nHVSupplies << ")!";
        }

        i++;
      } // End of while over json array

      break;
    }

  default:
    // HVSupplies.resize(8);
    HVSupplies[0]=0;
    break;
  } 
}
