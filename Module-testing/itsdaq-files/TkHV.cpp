// 
// TkHV.cpp:  
//   Class to facilitate VISA control of 
//   Keithley 487 picoammeter/voltage source 
//   or 
//   Keithley 2410 source/meter 
//   from ROOT
//
//   Additions for Keithley 2657A by Nathan Kang, SCIPP, 18/07/2017
//
//   The ITK HV supply is also supported (2022)  
//
// PWP 17/04/2013


#include "TkHV.h"
#include "../stlib/st_error.h"

#include <string>
#include <iostream>

#include <Riostream.h>
#include <Rtypes.h>

#include "../worker/worker.h"

#include "../serial/TSerialProxy.h"

/**
 * @brief Short timeout for making sure that there is nothing waiting or just arriving at the serial port.
 *
 * ITKHV produces a lot of confirmatory output (e.g. "ramping volts...", " Done ") which is
 * useful if the user is controlling the power supply directly or for confirming that operations have succeeded, but
 * not for this class. To get rid of it, Flush(VERY_SHORT_TIMEOUT) is called at various points in the code.
 * There is almost certainly a better way of achieving this.
 */
constexpr static Int_t VERY_SHORT_TIMEOUT = 5; // ms

constexpr static int ITKHVModel = 9999; // for iModel

std::string TkHV::deviceTypeName(DeviceType dt) {
  // using TkHV::DeviceType;
  switch(dt) {
  case DeviceType::SCPI_COMPLIANT: return "SCPI";
  case DeviceType::LEGACY: return "LEGACY";
  case DeviceType::ITKHV: return "ITKHV";
  default: return "UNKNOWN";
  }
}

class TkHVPrivate : public TSerialProxy {
public:
  ViUInt32 job;
  ViUInt32 rcount;

  Bool_t CheckError(const char* source, ViStatus status,
                    const char *sModel, const char *m_resource);

};

#define viRead port->viRead
#define viSetAttribute port->viSetAttribute
#define viWrite port->viWrite

#define job port->job
#define rcount port->rcount

#define CheckError(a, b) port->CheckError(a, b, sModel, m_resource)

#ifdef _WIN32
#undef max
#define LargeIntegerSubtract(a,b) (a.QuadPart - b.QuadPart)

static void _usleep(int us){
  /*
   * A REVISED implementation of usleep for windows.
   * This version uses large integer math throughout.
   * PWP 20.09.10
   */

  LARGE_INTEGER lNow, lTarget, lFreq;

  QueryPerformanceCounter(&lNow);

  QueryPerformanceFrequency(&lFreq);
  lTarget.QuadPart = lFreq.QuadPart * ((double)us/1000000.0);
  lTarget.QuadPart += lNow.QuadPart;

  do{
    QueryPerformanceCounter(&lNow);
  }while(lTarget.QuadPart > lNow.QuadPart);

  return;
}
#else
#define _usleep usleep
#endif

ClassImp(TkHV)

//
// Constructor and Destructor
//

TkHV::TkHV(const char* resName, bool SCPI) : port(new TkHVPrivate) {
  // Open the specified resource, and given that it is (or is not) SCPI compliant, detect its identity
  // (Most labs have k2410 units, so SCPI is the default)
  // PWP 19/10/2015

  // typical resource names
  // for GPIB:  "GPIB0::24::INSTR";
  // for RS-232: "ASRL1::INSTR"

  Init(resName, "UNKNOWN", SCPI);
}

TkHV::TkHV(const char* resName, DeviceType deviceType) : port(new TkHVPrivate) {
  Init(resName, "UNKNOWN", deviceType);
}

TkHV::TkHV(const char* resName, const char* label, bool SCPI) : port(new TkHVPrivate) {
  Init(resName, label, SCPI);
}

TkHV::TkHV(const char* resName, const char* label, DeviceType deviceType) : port(new TkHVPrivate) {
  Init(resName, label, deviceType);
}

TkHV::TkHV(const char* resName, int output, const char* label, DeviceType deviceType) : port(new TkHVPrivate) {
  Init(resName, output, label, deviceType);
}

void TkHV::Init(const char* resName, const char* label, bool SCPI) {
  if (SCPI) Init(resName, label, DeviceType::SCPI_COMPLIANT);
  else Init(resName, label, DeviceType::LEGACY);
}

void TkHV::Init(const char* resName, const char* label, DeviceType deviceType) {
  Init(resName, 1, label, deviceType);
}

void TkHV::Init(const char* resName, int output, const char* label, DeviceType deviceType) {
  // Open the specified resource, and given that it is (or is not) SCPI compliant, detect its identity
  // (Most labs have k2410 units, so SCPI is the default)
  // PWP 19/10/2015

  // typical resource names
  // for GPIB:  "GPIB0::24::INSTR";
  // for RS-232: "ASRL1::INSTR"

  strcpy(m_resource, resName);
  outputBool = bool(output);

  std::cout << "TkHV opening resource " << m_resource << std::endl;
  strcpy(m_resource, resName);
  strcpy(sModel,"UNKNOWN");
  iModel = -1;
  isValid = false;
  ivGraph = 0;
  nPoints = 0;
  fDelay = -1;
  vSafety = 550; // limit ramp requests to this, may be changed by user

  hd = 0;
  do_abort = false;
  do_threaded_ramp = true;
  do_threaded_scan = true;
 
  // Open session to VISA resource manager
  ViStatus status = port->viOpenDefaultRM ();
  if (status < VI_SUCCESS){
    std::cout << "TTi::TTi Could not open a session to the VISA Resource Manager :-(" << std::endl;
    exit (EXIT_FAILURE);
  }

  // Open specified resource
  status = port->viOpen (m_resource, VI_NULL, VI_NULL);
  if (status < VI_SUCCESS) {
    std::cout << "TTi::TTi Could not open resource " << m_resource << std::endl;
    exit (EXIT_FAILURE);
  }

  const std::string where = "TkHV::Init(args)";

  // Set default 5s timeout 
  status = viSetAttribute(VI_ATTR_TMO_VALUE, kHV_TIMEOUT_LONG);
  if(true==CheckError("TkHV::TkHV viSetAttribute failed", status)) return;

  switch (deviceType) {
  case DeviceType::SCPI_COMPLIANT: // "Modern" instrument, for example Keithley 2410


    //Need to read output twice for ISEG SHR serial connections
    if (!sendCommand("*IDN?\n", where)) return;
    status = viRead(data, 1024, &job); 
    status = viRead(data, 1024, &job);
    
    if (CheckError("TkHV::TkHV viRead *IDN?", status)) return;
    //ISEG Information. Remove this if not using the ISEG SHR
    iModel = 1234;
    strcpy(sModel, "iseg");
    iMax = 6000; // uA
    vMax = 1000; // V
    nChannels = atoi(label);
      
    //std::cout << "These are the number of nChannels: " << nChannels << std::endl;


/*

    if (!sendCommands({"*CLS\n", "*IDN?\n"}, where)) return;

    status = viRead(data, 1024, &job);

    if (CheckError("TkHV::TkHV viRead (*IDN?)", status)) 
      return;

    {
    Char_t *token = strtok((char*)data,","); // KEITHLEY INSTRUMENTS INC.
    token = strtok(NULL,",");      // MODEL
    if(0==strncmp((char*)token,"MODEL 2410",10)){
      iModel = 2410;
      strcpy(sModel, "k2410");
      iMax = 21000; // uA
      vMax = 1100;	// V
    }
    if(0==strncmp((char*)token," Model 2657A",12)){
      iModel = 2657;
      strcpy(sModel, "k2657a");
      iMax = 120000; // uA (1500V @ 120mA or 3000V @ 20mA)
      vMax = 1500;	// V 
    }
    } // Scope containing token
*/
    break;

  case DeviceType::ITKHV: // ITKHV supply 
      viSetAttribute(VI_ATTR_ASRL_RTS_STATE, 1);
      viSetAttribute(VI_ATTR_ASRL_DTR_STATE, 1);
      viSetAttribute(VI_ATTR_ASRL_BAUD, 115200);
      // viSetAttribute(VI_ATTR_ASRL_FLOW_CNTRL, VI_ASRL_FLOW_NONE);
      // viSetAttribute(VI_ATTR_TERMCHAR_EN, 1);
      iModel = ITKHVModel;
      strcpy(sModel, "ITKHV");
      iMax = 19999; // uA (actual current sent to ITKHV multiplied by 1000 to account for ITKHV firmware)
      vMax = 707; // V
      FlushDeviceBuffer();

      if(true){ // I want to define some variables inside the switch statement

        // Toggle the expert mode
        sprintf(stringinput, "X\n");
        status = viWrite(reinterpret_cast<ViBuf>(stringinput), strlen(stringinput), &rcount);

        // It may be that expert mode was already on, and by toggling it is now off. 
        // If that is the case we need to turn it ON
        ViStatus read_status = viRead(data, 1024, &job);
        std::cout << reinterpret_cast<char*>(data) << std::endl;
        std::cout << "ITKHV read status: " << read_status << std::endl;

        // "data" should be either:
        // " expert mode = OFF\n" or " expert mode =  ON\n"
        if(strncmp(reinterpret_cast<char*>(data), " expert mode = OFF", 18) == 0){
          std::cout << "ITKHV expert mode is OFF. Toggling ON" << std::endl;
          status = viWrite(reinterpret_cast<ViBuf>(stringinput), strlen(stringinput), &rcount);

          // check the status again to make sure
          read_status = viRead(data, 1024, &job);
          std::cout << reinterpret_cast<char*>(data) << std::endl;
          std::cout << "ITKHV read status: " << read_status << std::endl;
        }

      }
      break;

  case DeviceType::LEGACY:
    // "Legacy" instrument, for example Keithley 236, 237 or 487
    if (!sendCommand("U0X\n", "TkHV::Init")) return;

    status = viRead(data, 1024, &job);
    if (status == VI_SUCCESS){
    // k237 or k487

      if(0==strncmp((char*)data,"236",3)){ // untested
        iModel = 236;
        strcpy(sModel,"k236");
        iMax = 100000; // uA
        vMax = 110;    // V
      }
      if(0==strncmp((char*)data,"237",3)){ // working
        iModel = 237;
        strcpy(sModel,"k237");
        iMax = 100000; // uA
        vMax = 1100;   // V
      }
      if(0==strncmp((char*)data,"487",3)){ // working
        iModel = 487;
        strcpy(sModel, "k487");
        iMax = 2000;   // uA
        vMax = 500;    // V
      }
    }else{
      CheckError("TkHV::TkHV viRead (U0X)", status);
      return;
    }
    break;
  default:
    std::cout << "Invalid DeviceType given for HV supply.\n";
    return;
  }

  if(strcmp(sModel, "UNKNOWN") == 0) {
    // Not valid
    std::cout << "TkHV::TkHV not found at resource "<< m_resource << "\n";
    return;
  }

  std::cout << "TkHV::TkHV Found " << sModel << " at resource "<< m_resource << std::endl;

  if(strncmp(label,"UNKNOWN",7)==0){
    strcpy(outLabel, m_resource);
  }else{
    strcpy(outLabel, label);
  }

  Bool_t ok = Init();
  ok = PrintStatus();
  (void)ok;

  isValid = true;
}

TkHV::~TkHV(){
  // Destructor
  // - release vi session handles
  // PWP 15/04/2013

  work_shutdownWorkerThreads();

  std::cout << "TkHV::~TkHV destructor called for " << sModel << " at resource " << m_resource << std::endl;
  port->viClose();
}

// Abort any running threads
void TkHV::Abort(){
  do_abort = true;
}

// Report true if any threads are running
Bool_t TkHV::Busy(){
  return work_isJobComplete(hd);
}

// Return pointer to IVGraph
TGraph* TkHV::GetIVGraph(int channel){
  if (ivGraph == nullptr) {
    ivGraph = new TGraph(nPoints,xPoints[channel],yPoints[channel]);
    sprintf(stringinput, "IV Data from %s at resource %s", sModel, m_resource);
    ivGraph->SetTitle(stringinput);
  }
  return ivGraph;
}

bool TkHV::readNum(const char* const input, double& num, int scale) {
  if (!sendCommand(input, "TkHV::readNum")) {
    return false;
  }

  if (CheckError("TkHV::readNum viRead", viRead(data, 1024, &job))) {
    return false;
  }

  data[job] = 0;
  unsigned char* dataPtr = data;
  while (*dataPtr != '\0' && !isdigit(*dataPtr)) ++dataPtr;
  num = static_cast<double>(atof(reinterpret_cast<char*>(dataPtr)) * scale);
  return true;
}


bool TkHV::sendCommand(const std::string& command, const std::string &where) {
  return !CheckError((where + " sendCommand (" + command + ")").c_str(),
                     // The cast below performs the equivalent of both const_cast and reinterpret_cast!!!
                     viWrite((ViBuf)command.c_str(), command.size(), &rcount));
}
//
// LOW LEVEL ROUTINES
//

// Initialise your device
Bool_t TkHV::Init(){
  // Send intialisation strings
  // - but only if the source output is OFF
  // PWP 17/04/2013 

  // Is the unit ON or OFF?
  if (!ReadSettings()) return false;

  // Configure timeout always
  ViStatus status = viSetAttribute(VI_ATTR_TMO_VALUE, kHV_TIMEOUT_LONG); // 5000mS
  if (true == CheckError("TkHV::Init viSetAttribute failed", status)) return false;

  // Clear error buffer
  if (!ClearErrors()) return false;

  const std::string where = "TkHV::Init";

  switch (state) {
    case DeviceState::OFF:
      std::cout << "TkHV::Init Device is OFF, proceeding with initialisation" << std::endl;

      switch(iModel){
        case 236:
        case 237:
          // RTFM if you want to understand this!
          if(iModel==237) {
            if (!sendCommand("V1Y0F0,0X", where)) {
              return false;
            }
          } else {
            // 236 has no 1100 V range, cannot enable it
            if (!sendCommand("Y0F0,0X", where)) {
              return false;
            }
          }

          if (!sendCommand("G5,2,0X", where)) {
            return false;
          }

          break;

        case 487:
          // RTFM if you want to understand this!
          if (!sendCommand("F0G0K0M0N0R0S1Y0Z0", where)) {
            return false;
          }
          break;


        case 1234:
          //Set all channel voltages to zero
	  // hard code :) 

	  for(int channel = 0; channel < nChannels; channel ++) {
	    sprintf(stringinput,":VOLT 0,(@%d)\r\n",channel);
	    std::cout << "setting all channels to zero: " << stringinput << std::endl;
	    if (!sendCommand(stringinput, where)) return false;
	    status = viRead(data, 1024, &job);
	    status = viRead(data, 1024, &job);
	    
	    if(CheckError("TkHV::Mon viRead (:MEAS:VOLT)", status)) return false;
          
	    //Set polarity to negative
	    sprintf(stringinput,":CONF:OUTPUT:POL n,(@%d)\r\n",channel);
	    if (!sendCommand(stringinput, where)) return false;
	    status = viRead(data, 1024, &job);
	    status = viRead(data, 1024, &job);
	    if(CheckError("TkHV::Mon viRead (:CONF:POL)", status)) return false;
	  }
          
          break;

        case 2410:
          // reset the instrument
          if (!sendCommands({
                "*RST\n",
                // Configure Sense Function - CONCURRENT ON
                "SENSE:FUNCTION:CONCURRENT 10\n",
                // Configure Sense Function - Enable V and I
                "SENSE:FUNCTION:ON \"VOLTAGE\", \"CURRENT\"\n",
                // Configure Sense Function - Disable R
                "SENSE:FUNCTION:OFF \"RESISTANCE\"\n",
                // Configure Source Mode - VOLTAGE
                "SOURCE:FUNCTION VOLTAGE\n",
                // Configure Source Mode - FIXED (not sweep)
                "SOURCE:VOLTAGE:MODE FIXED\n",
                // Configure Source Mode - AUTORANGE - CHANGED TO OFF
                "SENSE:CURRENT:RANGE:AUTO 0\n",
                // Configure Source Mode - AUTORANGE - CHANGED TO OFF
                "SOURCE:VOLTAGE:RANGE:AUTO OFF\n"}, where)) {
            return false;
          }

          // Configure Source Mode - DC (not pulsed)
          if(iModel==2430){
            if (!sendCommand("SOURCE:FUNCTION:SHAPE DC\n", where)) {
              return false;
            }
          }

          // Configure Source Mode - PROTECTION LIMIT to 500V
          if (!sendCommand("SOURCE:VOLTAGE:PROTECTION:LEVEL 1000\n", where)) {
            return false;
          }

          // Configure Source Delay
          SetDelay(kHV_DELAY_SHORT);

          // SET RANGE OF VOLTAGE MEASUREMENT (UP TO 1000V)
          if (!sendCommand("SOURCE:VOLTAGE:RANGE 1000\n", where)) {
            return false;
          }
        
          // Configure Auto Range Change Mode to SINGLE (do not range up upon compliance during delay phase) - THIS IS NOT THE PROBLEM (CS 151106)
          if (!sendCommand("SYSTEM:RCMODE SINGLE\n", where)) {
            return false;
          }

          // Measurement range will be set according to COMPLIANCE
          SetFilter(5);   // average of 5 readings
          //SetTerminals(0); // front
          //SetTerminals(1); // rear
          SetTerminals(outputBool);
          break;

        case 2657:

          // reset the instrument
          if (!sendCommands({
                "reset()\n",
                // Configure Current Measurement Display
                "display.smua.measure.func = display.MEASURE_DCAMPS\n",
                // Configure Source Mode - VOLTAGE
                "smua.source.func = smua.OUTPUT_DCVOLTS\n",
                // Configure Source Mode - DISABLE (not sweep)
                "smua.trigger.source.action = smua.DISABLE\n",
                // Configure Source Mode - AUTORANGE - CHANGED TO OFF
                "smua.measure.autorangei = smua.AUTORANGE_OFF\n",
                // Configure Source Mode - AUTORANGE - CHANGED TO OFF
                "smua.source.autorangev = smua.AUTORANGE_OFF\n",
                // Configure Source Mode - PROTECTION LIMIT to 1000V
                "smua.source.protectv = 1000\n"}, where)) {
            return false;
          }

          // Configure Source Delay
          SetDelay(kHV_DELAY_SHORT);

          // SET RANGE OF VOLTAGE MEASUREMENT (UP TO 1000V)
          if (!sendCommand("smua.source.rangev = 1000\n", where)) {
            return false;
          }

          // Measurement range will be set according to COMPLIANCE
          SetFilter(5);   // average of 5 readings
          break;
        case ITKHVModel:
          std::cout << "TkHV: Initialising ITKHV. Nothing to do." << std::endl;
          break;
        default:
          std::cout << "TkHV::Init Initialisation not yet implemented for " << sModel << std::endl;
          return false;
          break;
      }

      break;

    case DeviceState::ON:
      std::cout << "TkHV::Init Device is ON, initialisation skipped" << std::endl;

      return false;
    case DeviceState::TRIPPED:
      std::cout << "TkHV::Init Device is TRIPPED, initialisation skipped" << std::endl;
      return false;

    default:
      return false;
  }

  return true;
}

// Binding for threaded scan
class BindScan {
  TkHV *k;
  Float_t vStart;
  Float_t vStop;
  Float_t vStep;
  Float_t vEnd;
  Float_t iLimit;
  Int_t msDelay;
  Bool_t append;
  int nPowerBoards;
public:
  BindScan(TkHV *k, Float_t vStart, Float_t vStop, Float_t vStep, Float_t vEnd, Float_t iLimit, Int_t msDelay, Bool_t append, int nPB)
    : k(k), vStart(vStart), vStop(vStop), vStep(vStep), vEnd(vEnd), iLimit(iLimit), msDelay(msDelay), append(append), nPowerBoards(nPB)
  { }
  void exec() {
    k->IVScanImpl(vStart, vStop, vStep, vEnd, iLimit, msDelay, append, nPowerBoards);
  }
  static void sexec(void*bs) {
    ((BindScan*)bs)->exec();
  }
};

// Start IV scan in its own thread (blocking)
Bool_t TkHV::IVScan(Float_t vStart, Float_t vStop, Float_t vStep, Float_t vEnd, Float_t iLimit, Int_t msDelay, Bool_t append)
{
  int nPowerBoards = 0;
  if ((hd != 0) && (!work_isJobComplete(hd))) {
    std::cout << "TkHV::IVScanBG: Previous thread has not yet completed" << std::endl;
    return false;
  }
  do_abort = 0;
  static BindScan *bs = 0;
  if (do_threaded_scan) {
#if 1
    if (bs) delete bs;
    bs = new BindScan(this, vStart, vStop, vStep, vEnd, iLimit, msDelay, append, nPowerBoards);
    hd = work_scheduleJob(&BindScan::sexec, bs);
    std::cout << "Scheduled job " << hd << std::endl;
#else // Lambda doesn't quite work
    hd = work_scheduleJob([&](void*){this->IVScanImpl(vStart, vStop, vStep, vEnd, iLimit, msDelay, append, nPowerBoards); }, NULL);
#endif
    while (!work_isJobComplete(hd)) {
      gSystem->Sleep(10);
      // All GUI stuff happens in main thread
      gSystem->ProcessEvents();
    }
  }
  else {
    // Not threaded burst
    do_abort = 0;
    return IVScanImpl(vStart, vStop, vStep, vEnd, iLimit, msDelay, append, nPowerBoards);
  }
  return true;
}

// Start IV scan in its own thread (blocking) with powerboard
Bool_t TkHV::IVScanWithPB(Float_t vStart, Float_t vStop, Float_t vStep, Float_t vEnd, Float_t iLimit, Int_t msDelay, Bool_t append, int nPB)
{
  /***
   * Note units:
   *  vStart, vStop, vStep, vEnd: V
   *  iLimit: 
   *  msDelay: ms
   *  ***************/
  if ((hd != 0) && (!work_isJobComplete(hd))) {
    std::cout << "TkHV::IVScanBG: Previous thread has not yet completed" << std::endl;
    return false;
  }
  do_abort = 0;
  static BindScan *bs = 0;
  if (do_threaded_scan) {
#if 1
    if (bs) delete bs;
    bs = new BindScan(this, vStart, vStop, vStep, vEnd, iLimit, msDelay, append, nPB);
    hd = work_scheduleJob(&BindScan::sexec, bs);
    std::cout << "Scheduled job " << hd << std::endl;
#else // Lambda doesn't quite work
    hd = work_scheduleJob([&](void*){this->IVScanImpl(vStart, vStop, vStep, vEnd, iLimit, msDelay, append, nPB); }, NULL);
#endif
    while (!work_isJobComplete(hd)) {
      gSystem->Sleep(10);
      // All GUI stuff happens in main thread
      gSystem->ProcessEvents();
    }
  }
  else {
    // Not threaded burst
    do_abort = 0;
    return IVScanImpl(vStart, vStop, vStep, vEnd, iLimit, msDelay, append, nPB);
  }
  return true;
}

// Start IV scan in its own thread (non-blocking)
void TkHV::IVScanBG(Float_t vStart, Float_t vStop, Float_t vStep, Float_t vEnd, Float_t iLimit, Int_t msDelay, Bool_t append)
{
  if((hd!=0) && (!work_isJobComplete(hd))) {
    std::cout << "TkHV::IVScanBG: Previous thread has not yet completed" << std::endl;
    return;
  }
  do_abort = 0;
  static BindScan *bs = 0;
  int nPowerBoards = 0;
#if 1
  if (bs) delete bs;
  bs = new BindScan(this, vStart, vStop, vStep, vEnd, iLimit, msDelay, append, nPowerBoards);
  hd = work_scheduleJob(&BindScan::sexec, bs);
#else // Lambda doesn't quite work
  hd = work_scheduleJob([&](void*){this->IVScanImpl(vStart, vStop, vStep, vEnd, iLimit, msDelay, append, nPowerBoards); }, NULL);
#endif
}


// Execute IV scan
bool TkHV::IVScanImpl(Float_t vStart, Float_t vStop, Float_t vStep, Float_t vEnd, Float_t iLimit, Int_t msDelay, Bool_t append, int nPB){
  // Simple IV scans
  // PWP 15/04/2013

  Float_t iLimitTemp = iLimit;

  if(append == false){
    nPoints = 0;
  }
  if(ivGraph){
    delete ivGraph;
    ivGraph =0;
  }

  if(vStep>0){
    // supplied as positive increment
    // flip polarity if vStop<vStart
    if(vStop<vStart) vStep = -vStep;
  }else{
    // supplied as negative increment
    // flip polarity if vStop>vNow
    if(vStop>vStart) vStep = -vStep;
  }

  int i=0;
  Bool_t ok = false;
  Bool_t done = false;
  Bool_t hitIt = false;
  Float_t vRequest = vStart;
  Int_t nReadings = 1;
  Int_t nRepeat = 3;
  while((!done)&&(!do_abort)&&(!hitIt)){
    if(i>0){
      vRequest += vStep;
    }
    if(vStep>0){
      if(vRequest>=vStop){
        vRequest = vStop;
        done = true;
      }
    }else{
      if(vRequest<=vStop){
        vRequest = vStop;
        done = true;
      }
    }
    // if powerboard, change compliance
    if (nPB > 0) {
      iLimitTemp = CalcPBCompliance(vRequest,iLimit,nPB);
    }
    ok = RampImpl(vRequest,iLimitTemp,3);
    if (!ok) return false;

    // repeat stability test at stop voltage
    if (done) {
      nReadings += nRepeat;
    }

    for (Int_t iReading = 0; iReading < nReadings; iReading++) {
      if (iReading == 1) std::cout << "TkHV::IVScan repeating stop voltage for " << nRepeat << " more measurements" << std::endl;
      _usleep(1000*msDelay);
      ok = Mon();
      
      
      if (!ok) return false;
      hitIt = HitCompliance();
      if(nPoints<MAX_IV_POINTS){
        for(int channel = 0; channel < nChannels; channel++){
          std::cout << "isegVMon: " << isegVMon[channel] << " isegIMon: " << isegIMon[channel] << std::endl;
          
          xPoints[channel][nPoints] = isegVMon[channel];
          yPoints[channel][nPoints] = isegIMon[channel];
          
          std::cout << "isegVMon xpoints: " << isegVMon[channel] << " isegIMon ypoints: " << isegIMon[channel] << std::endl;
          
//          xPoints[nPoints]=vMon;
//          yPoints[nPoints]=iMon;
        }
      }
      nPoints++;
      i++;
      std::cout << nPoints << " " << vStart << " " << vStop << " " << vMon << " " << iMon << std::endl;
    }
  }

  if((vEnd!=vStop)&&(!do_abort)&&(!hitIt)){
    // first ramp to end voltage with highest iLimit from vStop and vEnd
    if (nPB > 0) {
      iLimitTemp = std::max(CalcPBCompliance(vEnd,iLimit,nPB),CalcPBCompliance(vStop,iLimit,nPB));
    }
    ok = RampImpl(vEnd,iLimitTemp,3);
    if (!ok) return false;
    // now set the final iLimit correctly (if powerboard used)
    if (nPB > 0) {
      iLimitTemp = CalcPBCompliance(vEnd,iLimit,nPB);
      ok = RampImpl(vEnd,iLimitTemp,3);
      if (!ok) return false;
    }
  }

  for(int channel = 0; channel < nChannels; channel++){
    std::cout << "channel: " << channel << " xpoints: " << xPoints[channel] << " ypoints: " << yPoints[channel] << std::endl;
    ivGraph = new TGraph(nPoints,xPoints[channel],yPoints[channel]);
    sprintf(stringinput, "IV Data from %s at resource %s", sModel, m_resource);
    ivGraph->SetTitle(stringinput);
    ivGraph->Print();
  }

  if((hitIt)||(do_abort)){
    std::cout << "TkHV::IVScan aborted";
    if(hitIt)    std::cout << " (hit compliance)";
    if(do_abort) std::cout << " (user request)";
    if (!ok)     std::cout << " (error)";
  }else{
    std::cout << "TkHV::IVScan completed";
  }
  std::cout << " for " << sModel << " at resource " << m_resource << std::endl;

  return ok;
}

// If powerboard, change compliance for IV scan
Float_t TkHV::CalcPBCompliance(Float_t voltage, Float_t iLimit, int nPB){
  Float_t iLimitTemp = iLimit;

  iLimitTemp = fabs(voltage)/10*nPB + iLimit;

  return iLimitTemp;
}


/*

// Execute IV scan
bool TkHV::IVScanImpl(Float_t vStart, Float_t vStop, Float_t vStep, Float_t vEnd, Float_t iLimit, Int_t msDelay, Bool_t append, int nPB){
  // Simple IV scans
  // PWP 15/04/2013

  Float_t iLimitTemp = iLimit;

  if(append == false){
    nPoints = 0;
  }
  if(ivGraph){
    delete ivGraph;
    ivGraph =0;
  }

  if(vStep>0){
    // supplied as positive increment
    // flip polarity if vStop<vStart
    if(vStop<vStart) vStep = -vStep;
  }else{
    // supplied as negative increment
    // flip polarity if vStop>vNow
    if(vStop>vStart) vStep = -vStep;
  }

  int i=0;
  Bool_t ok = false;
  Bool_t done = false;
  Bool_t hitIt = false;
  Float_t vRequest = vStart;
  Int_t nReadings = 1;
  Int_t nRepeat = 3;
  while((!done)&&(!do_abort)&&(!hitIt)){
    if(i>0){
      vRequest += vStep;
    }
    if(vStep>0){
      if(vRequest>=vStop){
        vRequest = vStop;
        done = true;
      }
    }else{
      if(vRequest<=vStop){
        vRequest = vStop;
        done = true;
      }
    }
    // if powerboard, change compliance
    if (nPB > 0) {
      iLimitTemp = CalcPBCompliance(vRequest,iLimit,nPB);
    }
    ok = RampImpl(vRequest,iLimitTemp,3);
    if (!ok) return false;

    // repeat stability test at stop voltage
    if (done) {
      nReadings += nRepeat;
    }

    for (Int_t iReading = 0; iReading < nReadings; iReading++) {
      if (iReading == 1) std::cout << "TkHV::IVScan repeating stop voltage for " << nRepeat << " more measurements" << std::endl;
      _usleep(1000*msDelay);
      ok = Mon();
      if (!ok) return false;
      hitIt = HitCompliance();
      if(nPoints<MAX_IV_POINTS){
        xPoints[nPoints]=vMon;
        yPoints[nPoints]=iMon;
      }
      nPoints++;
      i++;
      std::cout << nPoints << " " << vStart << " " << vStop << " " << vMon << " " << iMon << std::endl;
    }
  }

  if((vEnd!=vStop)&&(!do_abort)&&(!hitIt)){
    // first ramp to end voltage with highest iLimit from vStop and vEnd
    if (nPB > 0) {
      iLimitTemp = std::max(CalcPBCompliance(vEnd,iLimit,nPB),
                            CalcPBCompliance(vStop,iLimit,nPB));
    }
    ok = RampImpl(vEnd,iLimitTemp,3);
    if (!ok) return false;
    // now set the final iLimit correctly (if powerboard used)
    if (nPB > 0) {
      iLimitTemp = CalcPBCompliance(vEnd,iLimit,nPB);
      ok = RampImpl(vEnd,iLimitTemp,3);
      if (!ok) return false;
    }
  }

  ivGraph = new TGraph(nPoints,xPoints,yPoints);
  sprintf(stringinput, "IV Data from %s at resource %s", sModel, m_resource);
  ivGraph->SetTitle(stringinput);
  ivGraph->Print();

  if((hitIt)||(do_abort)){
    std::cout << "TkHV::IVScan aborted";
    if(hitIt)    std::cout << " (hit compliance)";
    if(do_abort) std::cout << " (user request)";
    if (!ok)     std::cout << " (error)";
  }else{
    std::cout << "TkHV::IVScan completed";
  }
  std::cout << " for " << sModel << " at resource " << m_resource << std::endl;

  return ok;
}


*/

// If powerboard, change compliance for IV scan
//Float_t TkHV::CalcPBCompliance(Float_t voltage, Float_t iLimit, int nPB){
//  Float_t iLimitTemp = iLimit;

//  iLimitTemp = fabs(voltage)/10*nPB + iLimit;

//  return iLimitTemp;
//}

// Write settings and monitored data to previously opened log file
void TkHV::Log(FILE* outfile){
  // PWP 15/04/2013

  if(outfile==NULL){
    std::cout << "TkHV::Log called with null file" << std::endl;
    return;
  }

  Bool_t ok = ReadSettings();

  if(ok){
    ok = Mon();
  }

  if(ok == true){
    fprintf(outfile, "%%HV_INFO\n");
    fprintf(outfile, "#RESOURCE\tMODEL\tCHANNEL\n%s\t%s\t%d\n", m_resource, sModel, 0);
    fprintf(outfile, "#VSET\tISET\tVMON\tIMON\n%1.2f\t%1.2f\t%1.2f\t%1.2f\n", vSet, iSet, vMon, iMon);
  }

  return;
}

// Write IV scan data to previously opened log file
// TODO - seems to cause a crash if called from interpreter :-(
void TkHV::LogIV(FILE* outfile){
  // PWP 17/04/2013

  if(outfile==NULL){
    std::cout << "TkHV::LogIV called with null file" << std::endl;
    return;
  }

  Bool_t ok = ReadSettings();

  if(ok){
    ok = Mon();
  }

  if(ok == true){
    fprintf(outfile, "#V\tI\n");
    for(Int_t i=0; i<nPoints; i++){
      fprintf(outfile, "%3.1f\t%1.2f\n",xPoints[i],yPoints[i]);
    }
    fprintf(outfile, "#V\tuA\n");
  }

  return;
}

// Read monitored parameters and store internally
Bool_t TkHV::Mon(){
	// PWP 15/04/2013

  if(state != DeviceState::ON) {
    std::cout << "TkHV::Mon for " << sModel << " at resource " << m_resource;
    std::cout <<  " label " << outLabel << std::endl;
    std::cout << "Cannot Monitor values if source output is off. Returning zeros.\n";
    vMon = 0;
    iMon = 0;
    return true; // not really an error is it...
  }

  ViStatus status = VI_SUCCESS;

  std::string where = "TkHV::Mon";

  switch(iModel){
    Char_t *token;

    case 236:
    case 237:
      if (!sendCommand("H0X", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::Mon viRead (H0X)",status))return false;

      // Response takes the form of "VOLTS,CURRENT" for format G5,2,0
      data[job]=0;
      token = strtok((char*)data,",");
      vMon = atof((char*) token);
      token = strtok(NULL,"");
      iMon = 1000000 * atof((char*) token); // convert from A to uA
      break;

    case 487:
      // Read voltage setting (no direct monitor)
      if (!sendCommand("U8X", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::Mon viRead (U8X)",status))return false;

      // Response takes the form of "VS=-010.50E+00V"
      data[job]=0;
      token = strtok((char*)data,"VS=");
      vMon = atof((char*) token);

      // Read output current
      if (!sendCommand("T5X", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::Mon viRead (T5X)",status))return false;
  
      // Response takes the form of "NDCI+0.00000E-03"
      data[job]=0;
      token = strtok((char*)data,"NDCI+");
      iMon = 1000000 * atof((char*) token); // convert from A to uA
      break;

    case 1234:
      
      for(int channel = 0; channel < nChannels; channel++){
        
        sprintf(stringinput,":MEAS:VOLT? (@%d)\r\n",channel);
      
        if (!sendCommand(stringinput, where)) return false;
        status = viRead(data, 1024, &job);
	status = viRead(data, 1024, &job);
        
	
        if(CheckError("TkHV::Mon viRead (:MEAS:VOLT)", status)) return false;
        
        token = (char*)data;
        token[strlen(token) - 1] = '\0';
        isegVMon[channel] = atof((char*) token);
        
//        vMon = atof((char*) token);
        
        sprintf(stringinput,":MEAS:CURR? (@%d)\r\n",channel);
        if (!sendCommand(stringinput, where)) return false;
        status = viRead(data, 1024, &job);
        status = viRead(data, 1024, &job);
        
        if(CheckError("TkHV::Mon viRead (:MEAS:CURR)", status)) return false;
        
        token = (char*)data;
        token[strlen(token) - 1] = '\0';
        isegIMon[channel] = atof((char*) token)*1000000;
//        iMon = atof((char*) token)*1000000;
        
        std::cout << "ISEG channel " << channel << " has voltage: " << isegVMon[channel] << " and current: " << isegIMon[channel] << std::endl;
        
      }
      
      break;



    case 2410:
      if (!sendCommand(":READ?\n", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::Mon viRead (:READ?)",status))return false;

      // Response takes the form of "VOLTS,CURRENT,RESISTANCE,TIME,STATUS"
      data[job]=0;
      token = strtok((char*)data,",");
      vMon = atof((char*) token);
      token = strtok(NULL,",");
      if(token == NULL) {
        std::cout << "Failed to parse response '" << data << "'\n";
	    return false;
      }
      iMon = 1000000 * atof((char*) token); // convert from A to uA
      break;

    case 2657:
      if (!sendCommands({
              "iReading, vReading = smua.measure.iv()\n",
              // Read voltage
              "print(tostring(vReading))\n"}, where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::Mon viRead (print(tostring(vReading)))",status))return false;

      // Response is just the number
      data[job]=0;
      vMon = atof((char*)data);

      // Read current
      if (!sendCommand("print(tostring(iReading))\n", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::Mon viRead (print(tostring(iReading)))",status))return false;

      // Response is just the number
      data[job]=0;
      iMon = 1000000 * atof((char*)data); // convert from A to uA      
      break;      
    case ITKHVModel:
      FlushDeviceBuffer(VERY_SHORT_TIMEOUT);
      // Read output voltage (read in V, stored in V) and current (read in A, stored in uA)
      // Update 29/06/2022, current now read in uA we think (scale in readNum set to 1) 
      return readNum("v\n", vMon, -1) && readNum("i\n", iMon, 1);
    default:
      std::cout << "TkHV::Mon not yet implemented for " << sModel << std::endl;
      break;
  }

	return true;
}

// Set digital filter depth (number of samples to be averaged)
Bool_t TkHV::SetFilter(int nSamples){
  ViStatus status = VI_SUCCESS;

  std::string where = "TkHV::SetFilter";

  if(nSamples==0){
    switch(iModel){
      case 2410:
         // Disable Filter
        if (!sendCommand("SENS:AVER OFF\n", where)) {
          return false;
        }

        // Reduce timeout now filter is off
        status = viSetAttribute(VI_ATTR_TMO_VALUE, kHV_TIMEOUT_SHORT); // 500mS
        if(true==CheckError("TkHV::SetFilter viSetAttribute failed", status)) return false;
        break;

      case 2657:
         // Disable Filter
        if (!sendCommand("smua.measure.filter.enable = smua.FILTER_OFF\n", where)) {
          return false;
        }

        // Reduce timeout now filter is off
        status = viSetAttribute(VI_ATTR_TMO_VALUE, kHV_TIMEOUT_SHORT); // 500mS
        if(true==CheckError("TkHV::SetFilter viSetAttribute failed", status)) return false;
        break;

      default:
        std::cout << "TkHV::SetFilter not yet implemented for " << sModel << std::endl;
        return false;
        break;
    }

  }else{

    switch(iModel){
      case 2410:
        // Configure Filter Mode
        if (!sendCommand("SENS:AVER:TCON REP\n", where)) {
          return false;
        }

        // Configure Filter Count
        sprintf(stringinput,"SENS:AVER:COUN %d\n", nSamples); // OK
        if (!sendCommand(stringinput, where)) {
          return false;
        }
        
        // Enable Filter
        if (!sendCommand("SENS:AVER:STAT ON\n", where)) {
          return false;
        }

        // Increase timeout now filter is on
        status = viSetAttribute(VI_ATTR_TMO_VALUE, kHV_TIMEOUT_LONG); // 5000mS
        if(true==CheckError("TkHV::SetFilter viSetAttribute failed", status)) return false;
        break;

      case 2657:
        // Configure Filter Mode
        if (!sendCommand("smua.measure.filter.type = smua.FILTER_REPEAT_AVG\n",
                         where)) {
          return false;
        }

        // Configure Filter Count
        sprintf(stringinput,"smua.measure.filter.count = %d\n", nSamples);
        if (!sendCommand(stringinput, where)) {
          return false;
        }
        
        // Enable Filter
        if (!sendCommand("smua.measure.filter.enable = smua.FILTER_ON\n",
                         where)) {
          return false;
        }

        // Increase timeout now filter is on
        status = viSetAttribute(VI_ATTR_TMO_VALUE, kHV_TIMEOUT_LONG); // 5000mS
        if(true==CheckError("TkHV::SetFilter viSetAttribute failed", status)) return false;
        break;

      default:
        std::cout << "TkHV::SetFilter not yet implemented for " << sModel << std::endl;
        return false;
        break;
    }

  }
  return true;
}

// Set (software) safety limit
Bool_t TkHV::SetLimit(Float_t V){
  Float_t vAbs = abs(V);
  if (vAbs > vMax){
    std::cout << "TkHV::SetLimit: request exceeds possible range of " << -vMax << " to " << vMax << std::endl;
    return false;
  }
  if (vAbs < abs(vSet)){
    std::cout << "TkHV::SetLimit: request smaller than presently set value " << vSet << std::endl;
    return false;
  }

  vSafety = vAbs;
  std::cout << "TkHV::SetLimit: limit set to " << vSafety << std::endl;
  return true;
}

// Get (software) safety limit
Float_t TkHV::GetLimit(){
  return vSafety;
}

// Set active terminals (front or back)
Bool_t TkHV::SetTerminals(bool rear){

  switch(iModel){
    case 2410:
      // Configure rear terminals
      if (!sendCommand(rear?"ROUT:TERM REAR\n":
                            "ROUT:TERM FRONT\n", "TkHV::SetTerminals")) {
        return false;
      }

      break;
    case ITKHVModel:
      std::cout << "TkHV::SetTerminals: ITKHV only has one pair of terminals." << std::endl;
      return false;
    default:
      std::cout << "TkHV::SetTerminals not yet implemented for " << sModel << std::endl;
      return false;
  }

  return true;
}

// Read monitored values
Bool_t TkHV::Mon(unsigned char* Ustatus, Float_t* V, Float_t* I){
  // Wrapper to return monitored values
  // PWP 15/04/2013

  Bool_t ok = Mon();

  if(HitCompliance()) {
    *Ustatus = 1;
  } else {
    *Ustatus = 0;
  }

  *V = (Float_t)vMon;
  *I = (Float_t)iMon;

  return ok;
}

// Turn supply OFF
Bool_t TkHV::Off(){
  // PWP 15/04/2013

  ViStatus status = VI_SUCCESS;

  switch(iModel){
    case 236:
    case 237:
      sprintf(stringinput,"N0X");
      break;

    case 487:
      sprintf(stringinput,"O0X");
      break;

    case 1234:
      
      for(int channel = 0; channel < nChannels; channel++){
        
        sprintf(stringinput,":VOLT OFF,(@%d)\r\n",channel);
        
        if(!sendCommand(stringinput, "TkHV::Off")) return false;
        status = viRead(data, 1024, &job);
        status = viRead(data, 1024, &job);
        
        state = DeviceState::OFF;
      }
      break;


    case 2410:
      sprintf(stringinput,":OUTPUT OFF\n");
      break;

    case 2657:
      sprintf(stringinput,"smua.source.output = smua.OUTPUT_OFF\n");
      break;

    case ITKHVModel:
      /* For ITKHV, the output state must be read first because the only command available for changing it, E, toggles
       * the state (OFF to ON, ON to OFF, and TRIPPED to OFF).
       * We must hope that the state does not change between the e and E commands. */
      FlushDeviceBuffer(VERY_SHORT_TIMEOUT);
      double dState;
      if (!readNum("e\n", dState)) return false;
      state = DeviceState((int)(dState + 0.5));
      switch (state) {
        case DeviceState::OFF:
          std::cout << "TkHV::Off(): ITKHV is already off. No action taken." << std::endl;
          return false;
        case DeviceState::TRIPPED:
          std::cout << "TkHV::Off(): Warning: Resetting TRIPPED supply to OFF." << std::endl;
          // [[fallthrough]]; C++17
        case DeviceState::ON:
          if (!sendCommand("E\n", "TkHV::Off")) return false;
          state = DeviceState::OFF;
          FlushDeviceBuffer(VERY_SHORT_TIMEOUT);
          return true;
        default: return false;
      }
      sprintf(stringinput, "E\n");
      break;

    default:
      sprintf(stringinput,"VOID");
      break;
  }

  if(0!=strncmp(stringinput,"VOID",4)){
      if (!sendCommand(stringinput, "TkHV::Off")) {
        return false;
      }
      state = DeviceState::OFF;

      return true;
  }else{
      std::cout << "TkHV::Off not yet implemented for " << sModel << std::endl;
  }
  return false;
}

// Turn supply ON
Bool_t TkHV::On(){
  // PWP 15/04/2013

  ViStatus status = VI_SUCCESS;

  switch(iModel){
    case 236:
    case 237:
      sprintf(stringinput,"N1X");
      break;

    case 487:
      sprintf(stringinput,"O1X");
      break;

    case 1234:
      
      for(int channel = 0; channel < nChannels; channel++){
        sprintf(stringinput,":VOLT ON,(@%d)\r\n",channel);
        if (!sendCommand(stringinput, "TkHV::On")) return false;
        
	status = viRead(data, 1024, &job);
        status = viRead(data, 1024, &job);
        
        state = DeviceState::ON;
      }
      break;


    case 2410:
      sprintf(stringinput,":OUTPUT ON\n");
      break;

    case 2657:
      sprintf(stringinput,"smua.source.output = smua.OUTPUT_ON\n");
      break;        
    case ITKHVModel: {
      /* ITKHV: Output state must be read first because the only command available for changing it, E,
       * toggles the state (OFF to ON, ON to OFF, and TRIPPED to OFF).
       * We must hope that the state does not change between the e and E commands. */
      FlushDeviceBuffer(VERY_SHORT_TIMEOUT);
      double dState;
      if (!readNum("e\n", dState)) return false;
      state = DeviceState((int)(dState + 0.5));
      switch (state) {
        case DeviceState::OFF:
          if (!sendCommand("E\n", "TkHV::On")) {
            return false;
          }

          state = DeviceState::ON;
          FlushDeviceBuffer(VERY_SHORT_TIMEOUT);
          return true;
        case DeviceState::ON:
          std::cout << "TkHV::On(): ITKHV is already on. No action taken." << std::endl;
          return false;
        case DeviceState::TRIPPED:
          std::cout << "TkHV::On(): ITKHV is tripped. No action taken. Call TkHV::Off() to reset." << std::endl;
          return false;
        default: return false;
      }
      break; }
    default:
      sprintf(stringinput,"VOID");
      break;
  }

  if(0!=strncmp(stringinput,"VOID",4)){
      if (!sendCommand(stringinput, "TkHV::On")) {
          return false;
      }
      state = DeviceState::ON;

      return true;
  }else{
      std::cout << "TkHV::On not yet implemented for " << sModel << std::endl;
  }
  return false;
}

// Display status on console
Bool_t TkHV::PrintStatus(){
  // PWP 15/04/2013

  Bool_t ok = ReadSettings();
  if(ok==false) return ok;

  ok = Mon();
  
  std::cout << "TkHV::PrintStatus for " << sModel << " at resource " << m_resource << std::endl;
  std::cout << "  Label " << outLabel << std::endl;
  std::cout << "  Safety Limit (software) +/-" << vSafety << "V" << std::endl;
  std::cout << "  Max voltage " << vMax << "V" << std::endl;
  std::cout << "  Max current " << iMax << "uA" << std::endl;
  std::cout << "  Set voltage " << vSet << "V" << std::endl;
  if(iModel == ITKHVModel){
  // Note, for ITKHV, need to divide return values by 1000 
    std::cout << "  Set current " << iSet / 1000 << "uA" << std::endl;
  }
  else{
    std::cout << "  Set current " << iSet << "uA" << std::endl;
  }
  std::cout << "  Output state ";
  switch (state) {
  case DeviceState::ON:
    std::cout << "ON" << std::endl;
    std::cout <<  "  Output voltage " << vMon << "V" << std::endl;
    std::cout <<  "  Output current " << iMon << "uA" << std::endl;
    break;
  case DeviceState::OFF:
    std::cout << "OFF" << std::endl;
    break;
  case DeviceState::TRIPPED:
    std::cout << "TRIPPED" << std::endl;
    break;
  default:
    std::cout << " Unknown state in PrintStatus" << std::endl;
    break;
  }
  return ok;
}

// Binding for threaded ramp
class BindRamp {
  TkHV *k;
  Float_t vStop;
  Float_t iLimit;
  Int_t rampRate;
public:
  BindRamp(TkHV *k, Float_t vStop, Float_t iLimit, Int_t rampRate)
    : k(k), vStop(vStop), iLimit(iLimit), rampRate(rampRate)
  { }
  void exec() {
    k->RampImpl(vStop, iLimit, rampRate);
  }
  static void sexec(void*br) {
    ((BindRamp*)br)->exec();
  }
};

// Start ramp in its own thread (blocking)
Bool_t TkHV::Ramp(Float_t vStop, Float_t iLimit, Int_t rampRate)
{
  if ((hd) && (!work_isJobComplete(hd))) {
    std::cout << "TkHV::Ramp: Previous thread has not yet completed" << std::endl;
    return false;
  }
  do_abort = 0;
  static BindRamp *br = 0;
  if (do_threaded_ramp) {
#if 1
    if (br) delete br;
    br = new BindRamp(this, vStop, iLimit, rampRate);
    hd = work_scheduleJob(&BindRamp::sexec, br);
    std::cout << "Scheduled job " << hd << std::endl;
#else // Lambda doesn't quite work
    hd = work_scheduleJob([&](void*){this->RampImpl(vStop, iLimit, rampRate); }, NULL);
#endif
    while(!work_isJobComplete(hd)) {
      gSystem->Sleep(10);
      // All GUI stuff happens in main thread
      gSystem->ProcessEvents();
    }
  }
  else {
    // Not threaded ramp
    return RampImpl(vStop, iLimit, rampRate);
  }
  return true;
}

// Start ramp in its own thread (non-blocking)
void TkHV::RampBG(Float_t vStop, Float_t iLimit, Int_t rampRate)
{
  if((hd) && (!work_isJobComplete(hd))) {
    std::cout << "TkHV::RampBG: Previous thread has not yet completed" << std::endl;
    return;
  }
  do_abort = 0;
  static BindRamp *br = 0;
#if 1
  if (br) delete br;
  br = new BindRamp(this, vStop, iLimit, rampRate);
  hd = work_scheduleJob(&BindRamp::sexec, br);
  std::cout << "Job " << hd << "scheduled" << std::endl;
#else // Lambda doesn't quite work
  hd = work_scheduleJob([&](void*){this->RampImpl(vStop, iLimit, rampRate); }, NULL);
#endif
}

bool ItkHvRamp(float vStop, float iLimit, int rampRate, TkHV &p) {
    /* ITKHV ramps in firmware. We need only to issue commands to set the ramp rate and final voltage.
     * REAL_RAMP_RATES corresponds to the switch in the code for handling other supplies.
     *
     * Relevant commands for ITKHV are:
     *  U write the ramp Up rate in Volts per second
     *  D write the ramp Down rate in Volts per second
     *
     * */


    constexpr std::array<int, 4> REAL_RAMP_RATES = {50, 20, 10, 5}; // V/s
    char buffer[10];
    sprintf(buffer, "U%d\n", REAL_RAMP_RATES[rampRate - 1]); // Rate of ramping up
    if (!p.sendCommand(buffer, "ItkHvRamp")) return false;
    p.FlushDeviceBuffer(VERY_SHORT_TIMEOUT);

    sprintf(buffer, "D%d\n", REAL_RAMP_RATES[rampRate - 1]); // Rate of ramping down
    if (!p.sendCommand(buffer, "ItkHvRamp")) return false;
    p.FlushDeviceBuffer(VERY_SHORT_TIMEOUT);

    /* Set the final voltage. The supply will immediately begin ramping to it. If vStop == 0, the Set() call will block
     * until the voltage reaches zero so that the supply can then be turned Off(). */
    bool ok = p.Set(vStop, iLimit);
    if (!ok) return false;
    p.FlushDeviceBuffer(VERY_SHORT_TIMEOUT);
    return true;
}

// Execute Ramp
Bool_t TkHV::RampImpl(Float_t vStop, Float_t iLimit, int rampRate){
  // Revised ramp procedure
  // - ramp timings are inaccurate for 237 devices, but should be reasonable for 2410 units.
  // Rewritten PWP 15/04/2013 (sorry Carlos)

  // Special case: iLimit == -1 implies use existing setting
  if(iLimit==-1) iLimit = (Float_t) iSet;

  // Sanity checks
  if(iLimit<0.0){
    std::cout << "TkHV::RampImpl request rejected: " << iLimit << " below minimum level of 0uA" << std::endl;
    return false;
  }
  if(iLimit>iMax){
    std::cout << "TkHV::RampImpl request rejected: " << iLimit << " above maximum level of " << iMax << "uA" << std::endl;
    return false;
  }
  if (iModel == ITKHVModel) {
    // ITKHV can only produce negative voltages.
    if (vStop < -vMax || vStop > 0) {
      std::cout << "TkHV::RampImpl request rejected: V = " << vStop << " out of possible levels (-" << vMax << ", 0)" << std::endl;
      return false;
    }
    if (vStop < -vSafety) {
      std::cout << "TkHV::RampImpl request rejected: V = " << vStop << " out of permitted levels (-" << vSafety << ", 0)" << std::endl;
      return false;
    }
  } else {
    if ((vStop > vMax) || (vStop < -vMax)) {
      std::cout << "TkHV::RampImpl request rejected: V = " << vStop << " out of possible levels (" << -vMax << ", " << vMax << ")" << std::endl;
      return false;
    }
    if ((vStop > vSafety) || (vStop < -vSafety)) {
      std::cout << "TkHV::RampImpl request rejected: V = " << vStop << " out of permitted levels (" << -vSafety << ", " << vSafety << ")" << std::endl;
      return false;
    }
  }
  if ((rampRate<1) || (rampRate>4)){
    std::cout << "TkHV::RampImpl request rejected: rampRate = " << rampRate << " out of permitted range (1-4))" << std::endl;
	  return false;
  }
  if (((vStop>2.5) && (vSet<0)) || ((vStop<-2.5) && (vSet>0))){
    std::cout << "TkHV::RampImpl request rejected: polarity flip denied, ramp to zero first" << std::endl;
    return false;
  }

  Bool_t ok = ReadSettings();
  if(ok==false) return false;

  std::cout << "TkHV::RampImpl ramping to " << vStop << "V, rampRate " << rampRate << std::endl;

  // Apply long delay for ramping
  // EXCEPT when ramping to 0V (off)
  if(vStop==0) ok = SetDelay(kHV_DELAY_SHORT);
  else         ok = SetDelay(kHV_DELAY_LONG);
  if (ok == false) return false;

  if (state != DeviceState::ON){
    // Enable Source at 0V output
    ok = Set(0,iLimit);
    if(ok==false) return false;
    ok = ReadSettings();
    if(ok==false) return false;
    ok = On();
    if(ok==false) return false;
  }

  if (iModel == ITKHVModel) {
    return ItkHvRamp(vStop, iLimit, rampRate, *this);
  }

  Float_t vNow = (float) vSet;

  Float_t vStep;
  int tWait;
  switch(rampRate) {
  case 1: // 50V/s (approx)
    vStep = 12.5;
    tWait = 250000; // 0.25s
    break;
  case 2: // 20V/s (approx)
    vStep = 5.0;
    tWait = 250000; // 0.25s
    break;
  case 3: // 10V/s (approx)
    vStep = 2.50;
    tWait = 250000; // 0.25s
    break;
  case 4: // 5V/s (approx)
    vStep = 1.25;
    tWait = 250000; // 0.25s
    break;
  }

  if(vStep>0){
    // supplied as positive increment
    // flip polarity if vStop<vNow
    if(vStop<vNow) vStep = -vStep;
  }else{
    // supplied as negative increment
    // flip polarity if vStop>vNow
    if(vStop>vNow) vStep = -vStep;
  }

  Bool_t done = false;
  while( (!done) && (!do_abort) ){
    vNow+=vStep;
    if(vStep>0){
      if(vNow>=vStop){
        vNow = vStop;
        done = true;
      }
    }else{
      if(vNow<=vStop){
        vNow = vStop;
        done = true;
      }
    }
    ok = Set(vNow,iLimit);
    if(ok==false) return ok;
    _usleep(tWait); // Sleep is notoriously unreliable under windows!
    ok = Mon();
    if(ok==false) return ok;

    std::cout << "Requested "<< vNow << "V Achieved " << vMon << "V current " << iMon <<"uA" << std::endl;
  }

  // Apply short delay for monitoring
  ok = SetDelay(kHV_DELAY_SHORT);

  // Turn off if successfully reached 0V
  if((ok == true) && (vNow==0) && (!do_abort)){
    ok = Off();
  }

  // too much noise
  /*
  if(do_abort){
    std::cout << "TkHV::RampImpl aborted (user)";
  }
  else{
    if(ok) std::cout << "TkHV::RampImpl completed";
    else   std::cout << "TkHV::RampImpl aborted (error)";
  }
  std::cout << " at " << vNow << "V for " << sModel << " at resource " << m_resource << std::endl;
  */
  return ok;
}


Bool_t TkHV::RampDown(int speed) {
  // Compatibility Wrapper
  // PWP 15/04/2013

	return Ramp((float)0,-1,speed);
}


Bool_t TkHV::RampUp(int V, int speed) {
  // Compatibility Wrapper
  // PWP 15/04/2013

	return Ramp((float)V,-1,speed);
}

// Return label string
Bool_t TkHV::ReadLabel(char* label) const {
  // PWP 17/06/2013

  strcpy(label,outLabel);
  return true;
}

std::string TkHV::GetModel() const {
  return sModel;
}

std::string TkHV::GetResourceString() const {
  return m_resource;
}

std::string TkHV::GetSerialNumber() const {
  // Not read
  return "";
}

// Return upper limit values
Bool_t TkHV::ReadLimits(Float_t* V, Float_t* I){
  // PWP 15/04/2013

  *V = (Float_t)vMax;
  *I = (Float_t)iMax;
  return true;
}

// Read set values and store internally
Bool_t TkHV::ReadSettings(){
  // PWP 15/04/2013
  ViStatus status = VI_SUCCESS;

  std::string where = "TkHV::ReadSettings";

  switch(iModel){
    Char_t *token;

    case 236:
    case 237:
      // Read status byte 3
      if (!sendCommand("U3X", where)) {
        return false;
      }
  
      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::ReadSettings viRead (U3X)",status))return false;
  
      // Hunt the Nx portion if the status byte
      // - x is the output status
      //     0 -> OFF
      //     1 -> ON
      data[job]=0;
      token = strchr((char*)data,'N');
      //std::cout << token << std::endl;
      if(strlen(token)>2){
        switch(token[1]){
          case '0': state = DeviceState::OFF; break;
          case '1': state = DeviceState::ON; break;
          default:
            std::cout << "TkHV::ReadSettings read bad output state" << std::endl;
            break;
        }
      }

      // Read voltage
      if (!sendCommand("H0X", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::ReadSettings viRead (H0X)",status))return false;


      // Response takes the form of "VOLTS,CURRENT" for format G5,2,0
      data[job]=0;
      token = strtok((char*)data,",");
      vSet = atof((char*) token);

      // Read compliance
      if (!sendCommand("U5X", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::ReadSettings viRead (U5X)",status))return false;

      //Response takes the form of "ICPxxx.xxxE-YY
      data[job]=0;
      token = strtok((char*)data,"ICP");
      iSet = 1000000 * atof((char*) token);
      break;

    case 487:
      // Read Keithley Status
      if (!sendCommand("U0X", where)) {
        return false;
      }
  
      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::ReadSettings viRead (U0X)",status))return false;
  
      // Hunt the Ox portion if the status byte
      // - x is the output status
      //     0 -> OFF
      //     1 -> ON
      data[job]=0;
      token = strchr((char*)data,'O');
      std::cout << token << std::endl;
      if(strlen(token)>2){
        switch(token[1]){
          case '0': state = DeviceState::OFF; break;
          case '1': state = DeviceState::ON; break;
          default:
            std::cout << "TkHV::ReadSettings read bad output state" << std::endl;
            break;
        }
      }
    
      // Hunt the V portion if the status byte
      // - m is the voltage range 
      //     0 -> 50V
      //     1 -> 500V
      // - n is the current limit
      //     0 -> 25uA
      //     1 -> 2.5mA
      token = strchr((char*)data,'V');
      std::cout << token << std::endl;
      if(strlen(token)>2){
        switch(token[1]){
          case '0': // 50V
            break;
          case '1': // 500V
            break;
          default:
            std::cout << "TkHV::ReadSettings read bad V range " << token[1] << std::endl;
            break;
        }
        switch(token[2]){
          case '0': // 25uA
            iSet = 25;
            break;
          case '1': // 2.5mA
            iSet = 2500;
            break;
          default:
            iSet = -1;
            std::cout << "TkHV::ReadSettings read bad I range" << token[2] << std::endl;
            break;
        }
      }

      // Read voltage setting
      if (!sendCommand("U8X", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::ReadSettings viRead (U8X)", status))return false;

      // Response takes the form of "VS=-010.50E+00V"
      data[job]=0;
      token = strtok((char*)data,"VS=");
      vSet = atof((char*) token);
      break;

    case 1234:
      
      for(int channel = 0; channel < nChannels; channel++){
        
        sprintf(stringinput,":READ:VOLT:ON? (@%d)\r\n",channel);
        if (!sendCommand(stringinput, where)) return false;
        status = viRead(data, 1024, &job);
	status = viRead(data, 1024, &job);
        
        token = (char*)data;
        //Change this to off and add an on conditional
        if(atoi(token) == 0) {
          state = DeviceState::OFF;
        }
        if(atoi(token) == 1) {
          state = DeviceState::ON;
        }
        
        sprintf(stringinput,":MEAS:VOLT? (@%d)\r\n",channel);
        if (!sendCommand(stringinput, where)) return false;
        status = viRead(data, 1024, &job);
	status = viRead(data, 1024, &job);
        
        token = (char*)data;
        token[strlen(token) - 1] = '\0';
        vSet = atof(token);
        
        sprintf(stringinput,":MEAS:CURR? (@%d)\r\n",channel);
        if (!sendCommand(stringinput, where)) return false;
        status = viRead(data, 1024, &job);
        status = viRead(data, 1024, &job);
        
        token = (char*)data;
        token[strlen(token) - 1] = '\0';
        iSet = atof(token)*1000000; //Convert to uA
      }
      
      break;


    case 2410:

      // Query output state
      if (!sendCommand(":OUTPUT:STATE?\n", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::ReadSettings viRead (:OUTPUT:STATE?)",status))return false;

      // returns 0 or 1

      data[job]=0;
      if(atof((char*)data)==0) state = DeviceState::OFF;
      else state = DeviceState::ON;
      //if(0==strncmp((char*)data,"ON",2)) state = DeviceState::ON;
      //if(0==strncmp((char*)data,"OFF",3)) state = DeviceState::OFF;


      // Query compliance IN AMPS
      if (!sendCommand(":SENSE:CURRENT:PROTECTION:LEVEL?\n", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::ReadSettings viRead (:SENSE:CURRENT:PROTECTION:LEVEL?)",status))return false;
    
      // Response is just the number
      data[job]=0;
      iSet = 1000000 * atof((char*)data);
 
      // Query voltage
      if (!sendCommand(":SOURCE:VOLTAGE?\n", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::ReadSettings viRead (:SOURCE:VOLTAGE?)",status))return false;
    
      // Response is just the number
      data[job]=0;
      vSet = atof((char*)data);
 
      break;

    case 2657:

      // Query output state
      if (!sendCommand("print(tostring(smua.source.output))\n", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::ReadSettings viRead (print(tostring(smua.source.output)))",status))return false;

      // returns 0 (OFF) or 1 (ON), high Z mode also returns 0

      data[job]=0;
      if(atof((char*)data)==0) state = DeviceState::OFF;
      else state = DeviceState::ON;

      // Query compliance IN AMPS
      if (!sendCommand("print(tostring(smua.source.limiti))\n", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::ReadSettings viRead (print(tostring(smua.source.limiti)))",status))return false;
    
      // Response is just the number
      data[job]=0;
      iSet = 1000000 * atof((char*)data); //convert to uA 
 
      // Query voltage
      if (!sendCommand("print(tostring(smua.source.levelv))\n", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if(true==CheckError("TkHV::ReadSettings viRead (print(tostring(smua.source.levelv)))",status))return false;

      // Response is just the number
      data[job]=0;
      vSet = atof((char*)data);
 
      break;

    case ITKHVModel: {
      FlushDeviceBuffer(VERY_SHORT_TIMEOUT);
      { double dState;
        readNum("e\n", dState); // Query state
        state = DeviceState((int)(dState + 0.5));
      }
      readNum("s\n", vSet, -1); // Query set voltage. The value returned is the magnitude; the actual value is negative.
      break; }

    default:
      std::cout << "TkHV::ReadSettings not yet implemented for " << sModel << std::endl;
      return false;
  }

  return true;
}

// Return true if compliance limit has been reached
Bool_t TkHV::HitCompliance(){
  // PWP 15/07/2016

  Bool_t hitIt = false;

  std::string where = "TkHV::HitCompliance";

  ViStatus status = VI_SUCCESS;

  switch (iModel){

    case 1234:
      //std::cout << "No compliance implemented for iseg" << std::endl;
      break;

	  
    case 2410:
      // Query compliance trip
      if (!sendCommand(":SENSE:CURRENT:PROTECTION:TRIPPED?\n", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if (true == CheckError("TkHV::CheckCompliance viRead (:PROTECTION:TRIPPED?)", status))return false;

      if (atof((char*)data) == 1) hitIt = true;
      break;

    case 2657:
      // Query compliance trip
      if (!sendCommand("print(smua.source.compliance)\n", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if (true == CheckError("TkHV::CheckCompliance viRead (print(smua.source.compliance))", status))return false;

      data[job]=0;
      if (0==strncmp((char*)data,"true",4)) hitIt = true; // Returns true or false
      break;      
    case ITKHVModel:
      { double dState;
        readNum("e\n", dState);
        state = DeviceState((int)(dState + 0.5));
      }
      if (state == DeviceState::TRIPPED) hitIt = true;
      break;
    default:
      std::cout << "TkHV::CheckCompliance not yet implemented for " << sModel << std::endl;
      break;
  }

  return hitIt;
}


// Wrapper to return set values
Bool_t TkHV::ReadSettings(Float_t* V, Float_t* I){
  // PWP 15/04/2013

  Bool_t ok = ReadSettings();

  *V = (Float_t)vSet;
  *I = (Float_t)iSet;
  return ok;
}

// Set voltage and current limits
Bool_t TkHV::Set(Float_t voltage, Float_t current_uA){
  /* ****************
   * Expected units:
   * voltage : V
   * current_uA : uA
   * ****************/

  if(current_uA>iMax){
    std::cout << "TkHV::Set request rejected: " << current_uA << " above maximum level of " << iMax << "uA" << std::endl;
    return false;
  }
  if (iModel == ITKHVModel) {
    int itkhv_current_minimum_uA = 100;
    if(current_uA < itkhv_current_minimum_uA){
      std::cout << "TkHV::Set request rejected: " << current_uA << " below minimum level of " << itkhv_current_minimum_uA << " uA" << std::endl;
      std::cout << "TkHV::Set try setting the current limit to " << itkhv_current_minimum_uA << ", the ITK HV supply is not accurate below this value" << std::endl;
      return false;
    }
    if (voltage < -vMax || voltage > 0) {
      std::cout << "TkHV::Set request rejected: V = " << voltage << " out of possible levels (-" << vMax << ", 0)\n";
      return false;
    }
    if (voltage < -vSafety) {
      std::cout << "TkHV::Set request rejected: V = " << voltage << " out of permitted levels (-" << vSafety << ", 0)\n";
      return false;
    }
  } else {
    if(current_uA<0.0){
      std::cout << "TkHV::Set request rejected: " << current_uA << " below minimum level of 0uA" << std::endl;
      return false;
    }
    if ((voltage > vMax) || (voltage < -vMax)) {
      std::cout << "TkHV::Set request rejected: V = " << voltage << " out of possible levels (" << -vMax << ", " << vMax << ")\n";
      return false;
    }
    if ((voltage > vSafety) || (voltage < -vSafety)) {
      std::cout << "TkHV::Set request rejected: V = " << voltage << " out of permitted levels (" << -vSafety << ", " << vSafety << ")\n";
      return false;
    }
  }

  std::string where = "TkHV::Set";

  ViStatus status = VI_SUCCESS;


  switch(iModel){
    Float_t range;

    case 236:
    case 237:
      // Set compliance in AMPS, do not use autorange as this is slow

      range = 4;                               // 1uA
      if((current_uA>    1.0)&&(current_uA<=   10.0)) range = 5; //  10uA
      if((current_uA>   10.0)&&(current_uA<=  100.0)) range = 6; // 100uA
      if((current_uA>  100.0)&&(current_uA<= 1000.0)) range = 7; // 1mA
      if((current_uA> 1000.0)&&(current_uA<=10000.0)) range = 8; // 10mA
      if (current_uA>10000.0)                range = 9;

      sprintf(stringinput,"L%3.2e,%dX", current_uA/1000000, (int)range);
      //std::cout << "set1 " << stringinput << std::endl;
      if (!sendCommand(stringinput, where)) {
        return false;
      }

      // Set Output in VOLTS
      if(voltage>=0) sprintf(stringinput,"B%3.2e,%d,0X",voltage, ((voltage<=110.0f) ? 3:4) );
      else     sprintf(stringinput,"B%3.2e,%d,0X",voltage, ((voltage<=-110.0f) ? 4:3) );

      //std::cout << "set2 " << stringinput << std::endl;
      if (!sendCommand(stringinput, where)) {
        return false;
      }
      
      // No action taken until measurement is requested
      Mon();

      vSet = voltage;
      iSet = current_uA;
      break;

    case 487:
      sprintf(stringinput,"V%f,1,%d",voltage, ((current_uA<=25.0f) ? 0:1) );
      if (!sendCommand(stringinput, where)) {
        return false;
      }

      vSet = voltage;
      iSet = (current_uA<=25.0f) ? 25 : 2500;
      break;

    case 1234:
      
      //Setting voltage
      for(int channel = 0; channel < nChannels; channel++){
        sprintf(stringinput,":VOLT %.1f,(@%d)\r\n",voltage,channel);
        if (!sendCommand(stringinput, where)) return false;
        
        status = viRead(data, 1024, &job);
	status = viRead(data, 1024, &job);
	
	sprintf(stringinput,":CURR:BOUNDS %fE-6,(@%d)\r\n",current_uA,channel);
	std::cout<<stringinput<<std::endl;
   	if (!sendCommand(stringinput, where)) return false;
   	status = viRead(data, 1024, &job);
   	status = viRead(data, 1024, &job);
	sprintf(stringinput,":READ:CURR:BOUNDS? (@%d)\r\n",channel);
	if (!sendCommand(stringinput,where)) return false;
	status = viRead(data,1024, &job);
   	status = viRead(data, 1024, &job);
	Char_t *token;
	token = (char*)data;
	std::cout<<token<<std::endl;

        vSet = voltage;
        iSet = current_uA;
      }
      
      usleep(200000);
      
      break;


    case 2410:
      // Set current monitor range IN AMPS
      range = 0.00001;  // 10uA or lower
      if((current_uA>   10.0)&&(current_uA<=  100.0)) range = 0.0001;   // 100uA
      if((current_uA>  100.0)&&(current_uA<= 1000.0)) range = 0.001;    // 1mA
      if((current_uA> 1000.0)&&(current_uA<=10000.0)) range = 0.01;     // 10mA
      if (current_uA>10000.0)                range = 0.1;      // 100mA
      sprintf(stringinput,":SENS:CURR:RANGE %f\n", range);
      if (!sendCommand(stringinput, where)) {
        return false;
      }

      // Set compliance IN AMPS
      sprintf(stringinput,":SENSE:CURRENT:PROTECTION %f\n",current_uA/1000000);
      if (!sendCommand(stringinput, where)) {
        return false;
      }
 
      // Set voltage
      sprintf(stringinput,":SOURCE:VOLTAGE %f\n",voltage);
      if (!sendCommand(stringinput, where)) {
        return false;
      }

      vSet = voltage;
      iSet = current_uA;
      break;

    case 2657:
      // Set current monitor range IN AMPS
      range = 0.00001;  // 10uA or lower
      if((current_uA>   10.0)&&(current_uA<=  100.0)) range = 0.0001;   // 100uA
      if((current_uA>  100.0)&&(current_uA<= 1000.0)) range = 0.001;    // 1mA
      if((current_uA> 1000.0)&&(current_uA<=10000.0)) range = 0.01;     // 10mA
      if (current_uA>10000.0)                range = 0.1;      // 100mA or higher
      sprintf(stringinput,"smua.measure.rangei = %f\n", range);
      if (!sendCommand(stringinput, where)) {
        return false;
      }

      // Set compliance IN AMPS
      sprintf(stringinput,"smua.source.limiti = %f\n",current_uA/1000000);
      if (!sendCommand(stringinput, where)) {
        return false;
      }
 
      // Set voltage
      sprintf(stringinput,"smua.source.levelv = %f\n",voltage);
      if (!sendCommand(stringinput, where)) {
        return false;
      }

      vSet = voltage;
      iSet = current_uA;
      break;      

    case ITKHVModel:

      /* Set the output voltage for the ITKHV supply.
       * Relevant commands:
       * S:  Writes a new value to VSET. This value cannot be greater than VMAX. If the output is enabled the output volts will immediately ramp to the new value.
       *
       * Formatting of commands is e.g: "S200\n" ... will ramp to 200V 
       */

      // The supply produces negative voltages but takes their magnitudes as input, hence -v.
      sprintf(stringinput, "S%d\n", static_cast<int>(-voltage));
      if (!sendCommand(stringinput, where)) {
        return false;
      }
      vSet = voltage;
      FlushDeviceBuffer(VERY_SHORT_TIMEOUT);

      current_uA *= 1000; // This is important: Units conversion needed for the ITKHV firmware

      //sprintf(stringinput, "T%d\n", static_cast<int>(current_uA/1000000));
      sprintf(stringinput, "T%d\n", static_cast<int>(current_uA));
      if (!sendCommand(stringinput, where)) {
        return false;
      }
      iSet = current_uA;
      FlushDeviceBuffer(VERY_SHORT_TIMEOUT);

      // Just checking for vSet == 0 isn't enough, because Set() is called with voltage == 0 when ramping up begins.
      if (vSet == 0 && state == DeviceState::ON) {
        std::cout << "TkHV: Waiting for PSU to finish ramping to zero..." << std::endl;
        while (Mon(), std::abs(vMon) > 1) gSystem->Sleep(100);
        return Off();
      }
      break;
    default:
      std::cout << "TkHV::Set not yet implemented for " << sModel << std::endl;
      break;
  }

  return true;
}

// Program Source Delay if and only if a different value is required
Bool_t TkHV::SetDelay(Float_t delay) {
  // PWP 30/06/2016

  if (delay == fDelay) return true;

  std::string where = "TkHV::SetDelay";

  switch (iModel) {
  
  case 1234:
    break;
	  
  case 2410:
    sprintf(stringinput, "SOURCE:DELAY %1.1f\n", delay);
    if (!sendCommand(stringinput, where)) {
      return false;
    }
    break;

  case 2657:
    sprintf(stringinput, "smua.source.delay = %1.1f\n", delay);
    if (!sendCommand(stringinput, where)) {
      return false;
    }
    break;

  default:
    std::cout << "TkHV::SetDelay not yet implemented for " << sModel << std::endl;
    break;
  }

  fDelay = delay;
  return true;
}

//
// Private methods
//

Bool_t TkHV::ClearErrors() {
  // Read and clear any device errors
  // - return the number of errors cleared (if any)
  // PWP 30/06/2016

  Int_t nErrors = 0;

  std::string where = "TkHV::ClearErrors";

  ViStatus status = VI_SUCCESS;

  switch (iModel) {
    case 1234:
      break;
    case 2410:
      // Query error count
      if (!sendCommand(":SYSTEM:ERROR:COUNT?\n", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if (true == CheckError("TkHV::ReadSettings viRead (:SYSTEM:ERROR:COUNT?)", status))return false;

      data[job] = 0;
      nErrors = atoi((char*)data);

      std::cout << "TkHV::ClearErrors for " << sModel << " at resource " << m_resource << std::endl;
      std::cout << "  Device reports " << nErrors << " error(s):" << std::endl;

      for (Int_t i = 0; i < nErrors; i++) {
        // Query next error
        if (!sendCommand(":SYSTEM:ERROR:NEXT?\n", where)) {
          return false;
        }

        status = viRead(data, 1024, &job);
        if (true == CheckError("TkHV::ReadSettings viRead (:SYSTEM:ERROR:NEXT?)", status))return false;

        data[job] = 0; 
        std::cout << "  Error " << i << ": " << data << std::endl;
      }
      break;

    case 2657:
      // Query error count
      if (!sendCommand("print(tostring(errorqueue.count))\n", where)) {
        return false;
      }

      status = viRead(data, 1024, &job);
      if (true == CheckError("TkHV::ClearErrors viRead (print(tostring(errorqueue.count)))", status))return false;

      data[job] = 0;
      nErrors = atoi((char*)data);

      std::cout << "TkHV::ClearErrors for " << sModel << " at resource " << m_resource << std::endl;
      std::cout << "  Device reports " << nErrors << " error(s):" << std::endl;

      for (Int_t i = 0; i < nErrors; i++) {
        // Query next error
        if (!sendCommand("errorcode, message = errorqueue.next()\n", where)) {
          return false;
        }

        // Print the error code
        if (!sendCommand("print(tostring(errorcode))\n", where)) {
          return false;
        }

        status = viRead(data, 1024, &job);
        if (true == CheckError("TkHV::ClearErrors viRead (print(tostring(errorcode)))", status))return false;

        data[job] = 0; 
        std::cout << "  Error " << i+1 << ": errorcode " << data << std::endl;
        
        // Print the error message
        if (!sendCommand("print(tostring(message))\n", where)) {
          return false;
        }

        status = viRead(data, 1024, &job);
        if (true == CheckError("TkHV::ClearErrors viRead (print(tostring(message)))", status))return false;

        data[job] = 0;
        std::cout << "  - " << data << std::endl;
      }
      break;      

    default:
      std::cout << "TkHV::ClearErrors not yet implemented for " << sModel << std::endl;
      break;
  }

  return true;
}

void TkHV::FlushDeviceBuffer(Int_t timeout) {
  // Send LF to prompt supply to dump its buffer
  if (!sendCommand("\n", "TkHV: FlushDeviceBuffer")) return;

  // Keep reading lines from the buffer until it is empty
  ViStatus status = viSetAttribute(VI_ATTR_TMO_VALUE, timeout);
  if (CheckError( "FlushDeviceBuffer: viSetAttribute VI_ATTR_TMO_VALUE", status)) return;
  do {
    status = viRead(data, 1024, &job);
    if ((status < VI_SUCCESS) && (status != VI_ERROR_TMO)) return;
  } while (status >= VI_SUCCESS);

  viSetAttribute(VI_ATTR_TMO_VALUE, DEFAULT_TIMEOUT);
}
#undef CheckError

Bool_t TkHVPrivate::CheckError(const char* source, ViStatus status,
                               const char *m_resource, const char *sModel) {
  // Present VISA error codes in human readable, HIGHLIGHTED form ;-)
  // - return TRUE if error detected, else FALSE
  // PWP 08/07/2017

  if (status < VI_SUCCESS) {
    char mesg[1024];
    char desc[512];  //The size of the desc parameter should be at _least_ 256 bytes.
    viStatusDesc(status, desc);

    sprintf(mesg, "%s for %s at resource %s failed with code 0x%x (%s)\n", source, sModel, m_resource, status, desc);
    st_error(mesg);
    return true;
  }

  return false;
}

void TkHV::SetDebug(bool d) {
  TSerialProxy::setDebug(d);
}

