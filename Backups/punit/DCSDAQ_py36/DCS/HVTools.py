#import he one above?
#matplotlib.pyplot as plt
import numpy as np
import time
from db_manager import db_manager
import datetime
#from influxdb import InfluxDBClient

database=db_manager("influx",database="FourModuleBox")

def average_current(hv):
    current=0.0
    for i in range(5):
        current+=float(hv.GetCurrent())
        time.sleep(0.1)

    return current/5.0

#check if this should be +1000 or -1000
def IVCurve(hv,grafana_server,step=-5, end_limit=-450,begin_limit=0):
    
    hv.SetVoltageLevel(0)
    hv.ToRear()
    data_points=int(abs((int(end_limit)-int(begin_limit))/step))
    current_data=np.zeros(data_points,dtype=float)
    voltage_data=np.zeros(data_points,dtype=float)
    hv.SetVoltageRange(-100)
    database.init("http://"+grafana_server)
    #ramp up
    for i in range(data_points):        
        voltage_data[i]=float(hv.GetVoltage())
        current_data[i]=average_current(hv)*1000
        print(voltage_data[i],current_data[i])
        database.send("QCBox.IV_curve.voltage",float(voltage_data[i]))
        database.send("QCBox.IV_curve.current",float(current_data[i]))

        if(current_data[i]>=.0001*1000):
            end_limit=i*step
            curent_data.resize(i,False)
            voltage_data.resize(i,False)
            break
        
        time.sleep(2)
        hv.SetVoltageLevel((i+1)*step)
        time.sleep(2)
        



    #ramp down
    voltage=end_limit
    while voltage <=0:
        hv.SetVoltageLevel(voltage)        
        voltage=voltage-5*step
        time.sleep(1)
    if(voltage !=0):
        hv.SetVoltageLevel(0)
    hv.TurnOff()
    #ax=plt.subplot(111)
    dataa = np.array([voltage_data,current_data])
    dataa = dataa.T
    np.savetxt("IV_data.txt",dataa, delimiter=',')
    
    return ax
