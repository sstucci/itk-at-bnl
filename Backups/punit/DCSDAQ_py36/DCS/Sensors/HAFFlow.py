import time
import decimal
from decimal import*
from smbus import SMBus


class HAFFlow:

    def __init__(self,flowrange=200,bus=1,address=0x49):
        self.address=address
        self.bus=bus
        #print(type(SMBus(int(bus))))
        self.sensor=SMBus(int(bus))
        self.flowrange=flowrange
        
    def __del__(self):
        self.sensor.close()
        
    def read(self):
        #print("Ciao!")
        data=self.sensor.read_i2c_block_data(int(self.address,16), 0x00, 2)
        #print("Ciao2")
        #print(data)
        #print(data[0])
        #print(data[1])
        time.sleep(0.1)
        #print("ciao2")
        #data2=self.sensor.read_byte(int(self.address))
        #print(data2)
        
        
        output = data[0] << 8 | data[1]
        #print(output)
        #print(self.flowrange)
        #flow1 = ((200*output)/16384)
        #print(flow1)
        flow2= float(self.flowrange) * (((float(output)/float(16384)) - 0.1)/0.8) 
        print(flow2)
        conversionFactor = ( (273.15/293.15)*(14.696/14.504) )
        print(conversionFactor)
        flow= (flow2*conversionFactor)/28.3168;
        return flow        

    def get_data(self):
        print(self.read())
        return {"flow_rate":self.read()}
    
        

