import time
import smbus
from mux import multiplex
from adc import adc
import sys
import os

#check the address of the multiplexer
#os.system('i2cdetect -y 1')

#I'm assuming its 0x73, replace it if nexessary
pihat_plexer=multiplex(1,0x73)
pihat_adc=adc(vref=2.8,bus=1,address=0x33)

pihat_plexer.set_adc()

#check that adc was turned on
#os.system('i2cdetect -y 1')


NTC_voltage=[]
for channel in range(8):
    voltage=pihat_adc.read_voltage(channel)
    NTC_voltage.append(voltage)


add1=0x0c
add2=0x08
dac_write1=add1<<3
dac_write2=add2<<3
dac_write1|=6
dac_write2|=6
bus=smbus.SMBus(1)
for i in range(256):
    NTC_voltage=[]
    for channel in range(8):
        voltage=pihat_adc.read_voltage(channel)
        voltage=round(voltage,2)
        NTC_voltage.append(voltage)
    bus.write_i2c_block_data(add1,i,[dac_write1])
    bus.write_i2c_block_data(add1,i,[2048])
    bus.write_i2c_block_data(add1,i,[2048])

    bus.write_i2c_block_data(add2,i,[dac_write2])
    bus.write_i2c_block_data(add2,i,[2048])
    bus.write_i2c_block_data(add2,i,[2048])

    print(NTC_voltage)
    time.sleep(1)
